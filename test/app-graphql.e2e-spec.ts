import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { AppModule } from '../src/app.module';
import { AppModel } from '../src/app.resolver';
import { JwtAuthGuard } from '../src/authentication/guards/jwt-auth.guard';
import { SortOrderEnum } from '../src/common/enum/sort-order.enum';
import { SourceTypeEnum } from '../src/common/enum/source-type.enum';
import { CompanyModule } from '../src/company/company.module';
import { CompanyService } from '../src/company/company.service';
import { CompanyWithId } from '../src/company/models/company';
import { CompanySizeEnum } from '../src/company/schema/enum/company-size.enum';
import { CompanyStatusEnum } from '../src/company/schema/enum/company-status.enum';
import { ActiveDataType } from '../src/dashboard/@types/active-data.type';
import { NonActiveDataType } from '../src/dashboard/@types/non-active-data.type copy';
import { LastestJobDto } from '../src/dashboard/dtos/lastest-job.dto';
import { FavoriteWithId } from '../src/favorite/models/favorite.model';
import { CountryType } from '../src/job/@types/country.type';
import { JobModule } from '../src/job/job.module';
import { JobService } from '../src/job/job.service';
import { JobWithId } from '../src/job/models/job.model';
import { ExperienceLevelEnum } from '../src/job/schema/enum/experience-level.enum';
import { JobStatusEnum } from '../src/job/schema/enum/job-status.enum';
import { JobTypeEnum } from '../src/job/schema/enum/job-type.enum';
import { LocaitionWithId } from '../src/location/models/location.model';
import { PublisherWithId } from '../src/publisher/models/publisher.model';
import { PublisherModule } from '../src/publisher/publisher.module';
import { PublisherService } from '../src/publisher/publisher.service';
import { ApproveStatusEnum } from '../src/publisher/schema/enum/approve-status.enum';
import { ReviewWithId } from '../src/review/models/review.model';
import { ReviewModule } from '../src/review/review.module';
import { ReviewService } from '../src/review/review.service';
import { FavoriteService } from '../src/favorite/favorite.service';
import { UserWithId } from '../src/user/models/user.model';
import { EducationLevelEnum } from '../src/user/schema/enum/education.enum';
import { UserRoleEnum } from '../src/user/schema/enum/user-role.enum';
import { UserModule } from '../src/user/user.module';
import { FavoriteModule } from '../src/favorite/favorite.module';
import { UserService } from '../src/user/user.service';
import { WatchingListWithId } from '../src/watching-list/models/watching-list.model';
import {
  CREATE_COMPANY,
  CREATE_FAVORITE,
  CREATE_JOB,
  CREATE_PUBLISHER,
  CREATE_USER,
  CREATE_WATCHINGLIST,
  REMOVE_COMPANY,
  REMOVE_FAVORITE,
  REMOVE_JOB,
  REMOVE_REVIEW,
  REMOVE_WATCHINGLIST,
  UPDATE_COMPANY,
  UPDATE_JOB,
  UPDATE_PUBLISHER,
  UPDATE_USER,
} from './mutations';
import {
  GET_COMPANIES,
  GET_COMPANY,
  GET_COUNTRIES,
  GET_DASHBOARDSTAT,
  GET_FAVORITE,
  GET_FAVORITES,
  GET_JOB,
  GET_JOBS,
  GET_JOB_TREND,
  GET_LASTESTJOBSTAT,
  GET_LOCATION,
  GET_LOCATIONS,
  GET_PUBLISHER,
  GET_PUBLISHERS,
  GET_REVIEW,
  GET_REVIEWS,
  GET_SERVER_STATUS,
  GET_USER,
  GET_USERS,
  GET_WATCHINGLIST,
  GET_WATCHINGLISTS,
} from './queries';
import { ListTypeEnum } from '../src/watching-list/schema/enum/list-type.enum';
import { WatchingListService } from '../src/watching-list/watching-list.service';
import { WatchingListModule } from '../src/watching-list/watching-list.module';
import { UserSessionDTO } from '../src/authentication/dtos/user-session.dto';
import { ConfigService } from '../src/config/config.service';
import { ConfigModule } from '../src/config/config.module';
import { Request, Response } from 'express';
import { AuthenticationService } from '../src/authentication/authentication.service';
import { AuthenticationModule } from '../src/authentication/authentication.module';
import { UserLoginInputDTO } from '../src/authentication/dtos/user-login.input.dto';
import { PublisherSessionDTO } from '../src/authentication/dtos/publisher-session.dto';
import { UserResolver } from '../src/user/user.resolver';
import { PublisherResolver } from '../src/publisher/publisher.resolver';
import { AuthenticationResolver } from '../src/authentication/authentication.resolver';

describe('AppResolver GraphQL', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  // * Server status
  it('Query server status', () => {
    const expectedReturns: AppModel = { status: 200 };
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_SERVER_STATUS,
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.ServerStatus).toMatchObject<AppModel>({
          status: expect.any(Number),
        });
        expect(res.body.data).toEqual({
          ServerStatus: {
            ...expectedReturns,
          },
        });
      });
  });
});

describe('CompanyResolver GraphQL', () => {
  let app: INestApplication;
  const instanceVariable = {
    companyId: '',
  };
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('Mutation CreateCompany', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: CREATE_COMPANY,
        variables: {
          CreateCompanyInputDto: {
            description: 'integration_test',
            industries: ['Tech'],
            locations: ['Singapore'],
            name: 'integration_test',
            size: CompanySizeEnum.large,
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        instanceVariable.companyId = res.body.data.CreateCompany._id;
        expect(res.body.data.CreateCompany).toMatchObject({
          _id: instanceVariable.companyId,
          createAt: expect.any(String),
          description: 'integration_test',
          industries: ['Tech'],
          locations: ['Singapore'],
          logoImage: null,
          name: 'integration_test',
          properties: [],
          shortName: 'integration_test',
          size: CompanySizeEnum.large,
          sourceId: null,
          sourceType: SourceTypeEnum.internal,
          status: CompanyStatusEnum.pending,
          updateAt: expect.any(String),
        });
      });
  });

  it('Mutation UpdateCompany', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: UPDATE_COMPANY,
        variables: {
          UpdateCompanyInputDto: {
            id: instanceVariable.companyId,
            description: 'integration_test_updated',
            industries: ['Media'],
            locations: ['Canada'],
            name: 'integration_test_updated',
            size: CompanySizeEnum.medium,
            logoImage: 'https://thisislogo.png',
            status: CompanyStatusEnum.approved,
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.UpdateCompany).toMatchObject({
          _id: instanceVariable.companyId,
          createAt: expect.any(String),
          description: 'integration_test_updated',
          industries: ['Media'],
          locations: ['Canada'],
          logoImage: 'https://thisislogo.png',
          name: 'integration_test_updated',
          properties: [],
          shortName: 'integration_test_updated',
          size: CompanySizeEnum.medium,
          sourceId: null,
          sourceType: SourceTypeEnum.internal,
          status: CompanyStatusEnum.approved,
          updateAt: expect.any(String),
        });
      });
  });

  it('Mutation RemoveCompany', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: REMOVE_COMPANY,
        variables: {
          id: instanceVariable.companyId,
        },
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.RemoveCompany).toStrictEqual(1);
      });
  });

  // * Company module
  it('Query Company', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_COMPANY,
        variables: {
          id: '6204e813816570ee3a495c0a',
        },
      })
      .expect(200)
      .expect((res) =>
        expect(res.body.data.Company).toMatchObject<CompanyWithId>({
          _id: expect.any(String),
          sourceType: expect.any(String),
          sourceId: expect.any(String),
          description: expect.any(String),
          locations: expect.arrayContaining([expect.any(String)]),
          industries: expect.arrayContaining([expect.any(String)]),
          shortName: expect.any(String),
          name: expect.any(String),
          size: expect.any(String),
          logoImage: expect.any(String),
          // properties: CompanyProperty[],
          status: expect.any(String),
          createAt: expect.any(String),
          updateAt: expect.any(String),
        }),
      );
  });

  it('Query companies', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_COMPANIES,
        variables: {
          industry: 'Tech',
          limit: 1,
          location: 'Atlanta, GA',
          name: 'State Street',
          size: 'large',
          skip: 0,
          sortBy: 'createAt',
          sortOrder: 'desc',
        },
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.Companies.totalCount).toStrictEqual(
          expect.any(Number),
        );
        expect(res.body.data.Companies.companyResult).toMatchObject([
          {
            _id: expect.any(String),
            sourceType: expect.any(String),
            sourceId: expect.any(String),
            description: expect.any(String),
            locations: expect.arrayContaining([expect.any(String)]),
            industries: expect.arrayContaining([expect.any(String)]),
            shortName: expect.any(String),
            name: expect.any(String),
            size: expect.any(String),
            logoImage: expect.any(String),
            // properties: CompanyProperty[],
            status: expect.any(String),
            createAt: expect.any(String),
            updateAt: expect.any(String),
          },
        ]);
      });
  });

  it('Query Countries', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_COUNTRIES,
      })
      .expect(200)
      .expect((res) => {
        res.body.data.Countries.forEach((element) => {
          expect(element).toMatchObject<CountryType>({
            code: expect.any(String),
            name: expect.any(String),
          });
        });
      });
  });
});

describe('AuthenticationResolver GraphQL', () => {
  let app: INestApplication;
  let userService: UserService;
  let configService: ConfigService;
  let authenticationService: AuthenticationService;
  let authenticationResolver: AuthenticationResolver;
  let publisherService: PublisherService;
  let companyService: CompanyService;

  const newDob = new Date();
  const instanceVariable = {
    userId: '',
    publisherId: '',
    companyId: '',
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        UserModule,
        ConfigModule,
        AuthenticationModule,
        PublisherModule,
        CompanyModule,
      ],
      providers: [AuthenticationResolver],
    }).compile();
    userService = moduleFixture.get<UserService>(UserService);
    companyService = moduleFixture.get<CompanyService>(CompanyService);
    publisherService = moduleFixture.get<PublisherService>(PublisherService);
    configService = moduleFixture.get<ConfigService>(ConfigService);
    authenticationService = moduleFixture.get<AuthenticationService>(
      AuthenticationService,
    );
    authenticationResolver = moduleFixture.get<AuthenticationResolver>(
      AuthenticationResolver,
    );
    app = moduleFixture.createNestApplication();

    await app.init();
    // create test user
    const mockUserInput = {
      address: '165/76 Bangkok',
      email: 'integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      lastname: 'integration_test',
      password: 'integration_test',
      phonenumber: '0998885889',
      dob: newDob,
      description: 'integration_test',
    };
    const testUser = await userService.createUser(mockUserInput);
    instanceVariable.userId = testUser._id;

    // create company
    const mockCompanyInput = {
      description: 'integration_test',
      industries: ['Tech'],
      locations: ['Singapore'],
      name: 'integration_test',
      size: CompanySizeEnum.large,
      logoImage: 'https://integration_test.png',
    };
    const testCompany = await companyService.createCompany(mockCompanyInput);
    instanceVariable.companyId = testCompany._id;

    // create test publisher
    const mockPublisherInput = {
      companyId: instanceVariable.companyId,
      email: 'publisher_integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      job: 'integration_test',
      lastname: 'integration_test',
      password: '123qweasd',
      phonenumber: '0899999999',
    };
    const testPublisher = await publisherService.createPublisher(
      mockPublisherInput,
    );
    instanceVariable.publisherId = testPublisher._id;
    await publisherService.updatePublisher({
      id: instanceVariable.publisherId,
      status: ApproveStatusEnum.approved,
      email: 'publisher_integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      job: 'integration_test',
      lastname: 'integration_test',
      companyId: instanceVariable.companyId,
      password: '123qweasd',
      phonenumber: '0899999999',
    });
  });

  afterAll(async () => {
    const userDeletedCount = await userService.removeUser(
      instanceVariable.userId,
    );
    expect(userDeletedCount).toStrictEqual(1);

    const companyDeletedCount = await companyService.removeCompany(
      instanceVariable.companyId,
    );
    expect(companyDeletedCount).toStrictEqual(1);

    const publisherDeletedCount = await publisherService.removePublisher(
      instanceVariable.publisherId,
    );
    expect(publisherDeletedCount).toStrictEqual(1);
    await app.close();
  });

  it('Mutation Login', async () => {
    const mockReq = <Request>{
      session: {
        cookie: {
          path: '/',
          expires: new Date(Date.now() + configService.get().session.expires),
          originalMaxAge: configService.get().session.expires,
          httpOnly: true,
        },
      },
    };
    const userArgs = <UserLoginInputDTO>{
      email: 'integration_test@gmail.com',
      password: 'integration_test',
    };
    const res = await authenticationResolver.Login(mockReq, userArgs);
    expect(res).toMatchObject({
      _id: instanceVariable.userId,
      email: 'integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      lastname: 'integration_test',
      dob: newDob,
      phonenumber: '0998885889',
      address: '165/76 Bangkok',
      role: UserRoleEnum.user,
    });
    return;
  });

  it('Mutation LoginPublisher', async () => {
    const mockReq = <Request>{
      session: {
        cookie: {
          path: '/',
          expires: new Date(Date.now() + configService.get().session.expires),
          originalMaxAge: configService.get().session.expires,
          httpOnly: true,
        },
      },
    };
    const userArgs = <UserLoginInputDTO>{
      email: 'publisher_integration_test@gmail.com',
      password: '123qweasd',
    };
    const res = await authenticationResolver.LoginPublisher(mockReq, userArgs);
    expect(res).toMatchObject<PublisherSessionDTO>({
      _id: instanceVariable.userId,
      companyId: instanceVariable.companyId,
      email: 'publisher_integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      job: 'integration_test',
      lastname: 'integration_test',
      phonenumber: '0899999999',
      role: UserRoleEnum.publisher,
      status: ApproveStatusEnum.approved,
    });
    return;
  });

  it('Mutation Logout', async () => {
    const mockReq = <any>{
      session: {
        destroy: jest.fn(),
        cookie: {
          path: '/',
          expires: new Date(Date.now() + configService.get().session.expires),
          originalMaxAge: configService.get().session.expires,
          httpOnly: true,
        },
      },
    };
    const mockRes = (): Response => {
      const res: any = {};
      res.cookie = jest.fn().mockReturnValue(res);
      res.clearCookie = jest.fn();
      return res;
    };

    const res = await authenticationResolver.Logout(mockReq, mockRes());
    expect(res).toStrictEqual('logged out');
    expect(mockReq.session.destroy).toBeCalled();
    return;
  });

  it('Mutation RefreshToken', async () => {
    const mockReq = <any>{
      session: {
        touch: jest.fn(),
        user: {
          _id: instanceVariable.userId,
          email: 'integration_test@gmail.com',
          firstname: 'integration_test',
          middlename: 'integration_test',
          lastname: 'integration_test',
          dob: newDob,
          phonenumber: '0998885889',
          address: '165/76 Bangkok',
          role: UserRoleEnum.user,
        },
        cookie: {
          path: '/',
          expires: new Date(Date.now() + configService.get().session.expires),
          originalMaxAge: configService.get().session.expires,
          httpOnly: true,
        },
      },
    };

    const res = await authenticationResolver.RefreshToken(mockReq);
    expect(res).toStrictEqual('refreshed');
    expect(mockReq.session.touch).toBeCalled();
    return;
  });
});

describe('UserResolver GraphQL', () => {
  let app: INestApplication;
  let userService: UserService;
  let configService: ConfigService;
  let userResolver: UserResolver;

  const newDob = new Date();
  const instanceVariable = {
    createdUserId: '',
  };
  jest.setTimeout(30000);

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, UserModule, ConfigModule],
      providers: [UserResolver],
    })
      .overrideGuard(JwtAuthGuard)
      .useValue({ canActivate: () => true })
      .compile();
    userService = moduleFixture.get<UserService>(UserService);
    configService = moduleFixture.get<ConfigService>(ConfigService);
    userResolver = moduleFixture.get<UserResolver>(UserResolver);
    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    const deletedCount = await userService.removeUser(
      instanceVariable.createdUserId,
    );
    expect(deletedCount).toStrictEqual(1);
    await app.close();
  });

  // * User Module
  it('Mutation CreateUser', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: CREATE_USER,
        variables: {
          UserRegisterInputDto: {
            address: '165/76 Bangkok',
            email: 'integration_test@gmail.com',
            firstname: 'integration_test',
            middlename: 'integration_test',
            lastname: 'integration_test',
            password: 'integration_test',
            phonenumber: '0998885889',
            dob: newDob,
            description: 'integration_test',
          },
        },
      })
      .expect(200)
      .expect((res) => {
        instanceVariable.createdUserId = res.body.data.CreateUser._id;
        expect(res.body.data.CreateUser).toMatchObject({
          _id: instanceVariable.createdUserId,
          email: 'integration_test@gmail.com',
          firstname: 'integration_test',
          lastname: 'integration_test',
          middlename: 'integration_test',
          dob: newDob.toISOString(),
          phonenumber: '0998885889',
          address: '165/76 Bangkok',
          role: 'user',
          education: [],
          experience: [],
          skill: [],
          description: 'integration_test',
        });
      });
  });

  it('Query GetUserBySession', async () => {
    const mockReq: any = {
      session: {
        user: {
          _id: instanceVariable.createdUserId,
        },
        cookie: {
          path: '/',
          expires: new Date(Date.now() + configService.get().session.expires),
          originalMaxAge: configService.get().session.expires,
          httpOnly: true,
        },
      },
    };
    const res = await userResolver.GetUserBySession(mockReq);
    expect(JSON.parse(JSON.stringify(res))).toMatchObject({
      _id: instanceVariable.createdUserId,
      email: 'integration_test@gmail.com',
      firstname: 'integration_test',
      lastname: 'integration_test',
      middlename: 'integration_test',
      dob: newDob.toISOString(),
      phonenumber: '0998885889',
      address: '165/76 Bangkok',
      role: 'user',
      education: [],
      experience: [],
      skill: [],
      description: 'integration_test',
    });
    return;
  });

  it('Mutation UpdateUser', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: UPDATE_USER,
        variables: {
          UserUpdateInputDto: {
            id: instanceVariable.createdUserId,
            firstname: 'integration_test_updated',
            lastname: 'integration_test_updated',
            middlename: 'integration_test_updated',
            description: 'integration_test_updated',
            // education: {
            //   school: 'KMUTT',
            //   educationLevel: 'bachelor',
            //   graduationYear: 2022,
            // },
            // experience: [
            //   {
            //     name: 'Fullstack developer',
            //     year: 2,
            //     description: 'Work for my love company as developer manager.',
            //   },
            // ],
            skill: [{ name: 'python' }],
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.UpdateUser).toMatchObject({
          _id: instanceVariable.createdUserId,
          email: 'integration_test@gmail.com',
          firstname: 'integration_test_updated',
          lastname: 'integration_test_updated',
          middlename: 'integration_test_updated',
          dob: newDob.toISOString(),
          phonenumber: '0998885889',
          address: '165/76 Bangkok',
          role: UserRoleEnum.user,
          // education: [
          //   {
          //     school: 'KMUTT',
          //     educationLevel: EducationLevelEnum.bachelor,
          //     graduationYear: 2022,
          //   },
          // ],
          // experience: [
          //   {
          //     name: 'Fullstack developer',
          //     year: 2,
          //     description: 'Work for my love company as developer manager.',
          //   },
          // ],
          skill: [{ name: 'python' }],
          description: 'integration_test_updated',
        });
      });
  });

  it('Query User', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_USER,
        variables: {
          id: instanceVariable.createdUserId,
        },
      })
      .expect(200)
      .expect(async (res) => {
        if (res.body.data.User.education) {
          res.body.data.User.education.forEach((element) => {
            expect(element).toMatchObject({
              educationLevel: expect.any(String),
              graduationYear: expect.any(Number),
              school: expect.any(String),
            });
          });
        }
        if (res.body.data.User.experience) {
          res.body.data.User.experience.forEach((element) => {
            expect(element).toMatchObject({
              description: expect.any(String),
              name: expect.any(String),
              year: expect.any(Number),
            });
          });
        }
        if (res.body.data.User.skill) {
          res.body.data.User.skill.forEach((element) => {
            expect(element).toMatchObject({
              name: expect.any(String),
            });
          });
        }

        expect(res.body.data.User).toMatchObject<Partial<UserWithId>>({
          _id: expect.any(String),
          address: expect.any(String),
          createAt: expect.any(String),
          dob: expect.any(String),
          email: expect.any(String),
          firstname: expect.any(String),
          lastname: expect.any(String),
          phonenumber: expect.any(String),
          role: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });

  it('Query Users', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_USERS,
        variables: {
          limit: 2,
          skip: 0,
        },
      })
      .expect(200)
      .expect((res) => {
        res.body.data.Users.forEach((element) => {
          if (element.education) {
            element.education.forEach((e) => {
              expect(e).toMatchObject({
                educationLevel: expect.any(String),
                graduationYear: expect.any(Number),
                school: expect.any(String),
              });
            });
          }
          if (element.experience) {
            element.experience.forEach((e) => {
              expect(e).toMatchObject({
                description: expect.any(String),
                name: expect.any(String),
                year: expect.any(Number),
              });
            });
          }
          if (element.skill) {
            element.skill.forEach((e) => {
              expect(e).toMatchObject({
                name: expect.any(String),
              });
            });
          }

          expect(element).toMatchObject<Partial<UserWithId>>({
            _id: expect.any(String),
            address: expect.any(String),
            createAt: expect.any(String),
            dob: expect.any(String),
            email: expect.any(String),
            firstname: expect.any(String),
            lastname: expect.any(String),
            phonenumber: expect.any(String),
            role: expect.any(String),
            updateAt: expect.any(String),
          });
        });
      });
  });
});

describe('FavoriteResolver GraphQL', () => {
  let app: INestApplication;
  let userService: UserService;
  let jobService: JobService;
  let companyService: CompanyService;
  let favoriteService: FavoriteService;
  const newDob = new Date();
  const instanceVariable = {
    userId: '',
    jobId: '',
    companyId: '',
    companyName: '',
    favoriteId: '',
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        UserModule,
        JobModule,
        CompanyModule,
        FavoriteModule,
      ],
    })
      .overrideGuard(JwtAuthGuard)
      .useValue({ canActivate: () => true })
      .compile();
    userService = moduleFixture.get<UserService>(UserService);
    jobService = moduleFixture.get<JobService>(JobService);
    companyService = moduleFixture.get<CompanyService>(CompanyService);
    favoriteService = moduleFixture.get<FavoriteService>(FavoriteService);
    app = moduleFixture.createNestApplication();

    await app.init();

    // create test user
    const mockUserInput = {
      address: '165/76 Bangkok',
      email: 'integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      lastname: 'integration_test',
      password: 'integration_test',
      phonenumber: '0998885889',
      dob: newDob,
      description: 'integration_test',
    };
    const testUser = await userService.createUser(mockUserInput);
    instanceVariable.userId = testUser._id;

    // create test company
    const mockCompanyInput = {
      description: 'integration_test',
      industries: ['Tech'],
      locations: ['Singapore'],
      name: 'integration_test',
      size: CompanySizeEnum.large,
      logoImage: 'https://integration_test.png',
    };
    const testCompany = await companyService.createCompany(mockCompanyInput);
    instanceVariable.companyId = testCompany._id;
    instanceVariable.companyName = testCompany.name;

    // create test job
    const mockJobInput = {
      applyUrl: 'www.integration_test.com',
      benefits: ['integration_test'],
      categories: ['integration_test'],
      companyId: instanceVariable.companyId,
      companyName: instanceVariable.companyName,
      description: 'integration_test',
      educationLevel: EducationLevelEnum.bachelor,
      experienceLevels: [ExperienceLevelEnum.senior],
      locations: ['Bellevue, WA'],
      name: 'integration_test',
      remote: true,
      salary: '10000',
      contents: 'integration_test',
      requirements: ['integration_test'],
      shortName: 'integration_test',
      jobType: JobTypeEnum.external,
    };
    const testJob = await jobService.createJob(mockJobInput);
    instanceVariable.jobId = testJob._id;
  });

  afterAll(async () => {
    // remove test user
    const userDeleteCount = await userService.removeUser(
      instanceVariable.userId,
    );
    expect(userDeleteCount).toStrictEqual(1);

    // remove test company
    const companyDeleteCount = await companyService.removeCompany(
      instanceVariable.companyId,
    );
    expect(companyDeleteCount).toStrictEqual(1);

    // remove test job
    const jobDeleteCount = await jobService.removeJob(instanceVariable.jobId);
    expect(jobDeleteCount).toStrictEqual(1);

    // delete favorite
    const favoriteDeleteCount = await favoriteService.deleteFavorite(
      instanceVariable.favoriteId,
    );
    expect(favoriteDeleteCount).toStrictEqual(1);
    await app.close();
  });

  it('Mutation CreateFavorite', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: CREATE_FAVORITE,
        variables: {
          FavoriteJobInputDto: {
            userId: instanceVariable.userId,
            jobId: instanceVariable.jobId,
          },
        },
      })
      .expect(200)
      .expect((res) => {
        instanceVariable.favoriteId = res.body.data.CreateFavorite._id;
        expect(res.body.data.CreateFavorite).toMatchObject<FavoriteWithId>({
          _id: instanceVariable.favoriteId,
          jobsId: [instanceVariable.jobId],
          userId: instanceVariable.userId,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });

  it('Query Favorite', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_FAVORITE,
        variables: {
          id: instanceVariable.favoriteId,
        },
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.Favorite).toMatchObject<Partial<FavoriteWithId>>({
          _id: instanceVariable.favoriteId,
          jobsId: [instanceVariable.jobId],
          userId: instanceVariable.userId,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });

  it('Query Favorites', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_FAVORITES,
        variables: {
          userId: instanceVariable.userId,
        },
      })
      .expect((res) => {
        res.body.data.Favorites.forEach((element) => {
          expect(element).toMatchObject({
            _id: expect.any(String),
            applyUrl: expect.any(String),
            categories: expect.arrayContaining([expect.any(String)]),
            companyId: expect.any(String),
            companyName: expect.any(String),
            contents: expect.any(String),
            createAt: expect.any(String),
            experienceLevels: expect.arrayContaining([expect.any(String)]),
            jobType: expect.any(String),
            locations: expect.arrayContaining([expect.any(String)]),
            name: expect.any(String),
            remote: expect.any(Boolean),
            shortName: expect.any(String),
            sourceId: null,
            sourceType: expect.any(String),
            status: expect.any(String),
            updateAt: expect.any(String),
          });
        });
      });
  });

  it('Mutation RemoveFavorite', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: REMOVE_FAVORITE,
        variables: {
          FavoriteJobInputDto: {
            jobId: instanceVariable.jobId,
            userId: instanceVariable.userId,
          },
        },
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.RemoveFavorite).toMatchObject<FavoriteWithId>({
          _id: instanceVariable.favoriteId,
          jobsId: [],
          userId: instanceVariable.userId,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });
});

describe('JobResolver GraphQL', () => {
  let app: INestApplication;
  let companyService: CompanyService;
  const instanceVariable = {
    jobId: '',
    companyId: '',
    companyName: '',
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, CompanyModule],
    }).compile();
    companyService = moduleFixture.get<CompanyService>(CompanyService);
    app = moduleFixture.createNestApplication();

    await app.init();
    const mockCompanyInput = {
      description: 'integration_test',
      industries: ['Tech'],
      locations: ['Singapore'],
      name: 'integration_test',
      size: CompanySizeEnum.large,
      logoImage: 'https://integration_test.png',
    };
    const testCompany = await companyService.createCompany(mockCompanyInput);
    instanceVariable.companyId = testCompany._id;
    instanceVariable.companyName = testCompany.name;
  });

  afterAll(async () => {
    const deleteCount = await companyService.removeCompany(
      instanceVariable.companyId,
    );
    expect(deleteCount).toStrictEqual(1);
    await app.close();
  });

  it('Mutation CreateJob', async () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: CREATE_JOB,
        variables: {
          CreateJobInputDto: {
            applyUrl: 'www.integration_test.com',
            benefits: ['integration_test'],
            categories: ['integration_test'],
            companyId: instanceVariable.companyId,
            companyName: instanceVariable.companyName,
            description: 'integration_test',
            educationLevel: EducationLevelEnum.bachelor,
            experienceLevels: [ExperienceLevelEnum.senior],
            locations: ['Bellevue, WA'],
            name: 'integration_test',
            remote: 'true',
            salary: '10000',
            contents: 'integration_test',
            requirements: ['integration_test'],
            shortName: 'integration_test',
            jobType: JobTypeEnum.external,
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        instanceVariable.jobId = res.body.data.CreateJob._id;
        expect(res.body.data.CreateJob).toMatchObject<JobWithId>({
          _id: instanceVariable.jobId,
          applyUrl: 'www.integration_test.com',
          benefits: ['integration_test'],
          categories: ['integration_test'],
          companyId: instanceVariable.companyId,
          companyName: instanceVariable.companyName,
          description: 'integration_test',
          educationLevel: EducationLevelEnum.bachelor,
          experienceLevels: [ExperienceLevelEnum.senior],
          locations: ['Bellevue, WA'],
          name: 'integration_test',
          remote: true,
          salary: '10000',
          contents: 'integration_test',
          requirements: ['integration_test'],
          shortName: 'integration_test',
          jobType: JobTypeEnum.external,
          sourceType: SourceTypeEnum.internal,
          status: JobStatusEnum.normal,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });

  it('Mutation UpdateJob', async () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: UPDATE_JOB,
        variables: {
          UpdateJobInputDto: {
            id: instanceVariable.jobId,
            applyUrl: 'www.integration_test_updated.com',
            benefits: ['integration_test_updated'],
            categories: ['integration_test_updated'],
            description: 'integration_test_updated',
            educationLevel: EducationLevelEnum.doctoral,
            experienceLevels: [
              ExperienceLevelEnum.senior,
              ExperienceLevelEnum.mid,
            ],
            locations: ['Singapore'],
            name: 'integration_test_updated',
            remote: 'false',
            salary: '20000',
            contents: 'integration_test_updated',
            requirements: ['integration_test_updated'],
            shortName: 'integration_test_updated',
            jobType: JobTypeEnum.internal,
            sourceType: SourceTypeEnum.internal,
            status: JobStatusEnum.reported,
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.UpdateJob).toMatchObject<JobWithId>({
          _id: instanceVariable.jobId,
          applyUrl: 'www.integration_test_updated.com',
          companyId: instanceVariable.companyId,
          companyName: instanceVariable.companyName,
          benefits: ['integration_test_updated'],
          categories: ['integration_test_updated'],
          description: 'integration_test_updated',
          educationLevel: EducationLevelEnum.doctoral,
          experienceLevels: [
            ExperienceLevelEnum.senior,
            ExperienceLevelEnum.mid,
          ],
          locations: ['Singapore'],
          name: 'integration_test_updated',
          remote: false,
          salary: '20000',
          contents: 'integration_test_updated',
          requirements: ['integration_test_updated'],
          shortName: 'integration_test_updated',
          jobType: JobTypeEnum.internal,
          sourceType: SourceTypeEnum.internal,
          status: JobStatusEnum.reported,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });

  it('Query Job', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_JOB,
        variables: {
          id: instanceVariable.jobId,
        },
      })
      .expect(200)
      .expect((res) =>
        expect(res.body.data.Job).toMatchObject<JobWithId>({
          _id: instanceVariable.jobId,
          applyUrl: 'www.integration_test_updated.com',
          companyId: instanceVariable.companyId,
          companyName: instanceVariable.companyName,
          benefits: ['integration_test_updated'],
          categories: ['integration_test_updated'],
          description: 'integration_test_updated',
          educationLevel: EducationLevelEnum.doctoral,
          experienceLevels: [
            ExperienceLevelEnum.senior,
            ExperienceLevelEnum.mid,
          ],
          locations: ['Singapore'],
          name: 'integration_test_updated',
          remote: false,
          salary: '20000',
          contents: 'integration_test_updated',
          requirements: ['integration_test_updated'],
          shortName: 'integration_test_updated',
          jobType: JobTypeEnum.internal,
          sourceType: SourceTypeEnum.internal,
          status: JobStatusEnum.reported,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        }),
      );
  });

  it('Query Jobs', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_JOBS,
        variables: {
          category: 'integration_test_updated',
          companyId: instanceVariable.companyId,
          // jobType: JobTypeEnum.internal,
          limit: 1,
          // location: 'Singapore',
          // name: 'integration_test_updated',
          remote: false,
          // salary: '20000',
          skip: 0,
          sortBy: 'createAt',
          sortOrder: SortOrderEnum.desc,
          status: JobStatusEnum.reported,
        },
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.Jobs.totalCount).toStrictEqual(expect.any(Number));
        res.body.data.Jobs.jobResult.forEach((element) => {
          expect(element).toMatchObject({
            _id: expect.any(String),
            applyUrl: expect.any(String),
            categories: expect.arrayContaining([expect.any(String)]),
            companyId: expect.any(String),
            companyName: expect.any(String),
            contents: expect.any(String),
            createAt: expect.any(String),
            experienceLevels: expect.arrayContaining([expect.any(String)]),
            jobType: expect.any(String),
            locations: expect.arrayContaining([expect.any(String)]),
            name: expect.any(String),
            remote: expect.any(Boolean),
            shortName: expect.any(String),
            sourceId: null,
            sourceType: expect.any(String),
            status: expect.any(String),
            updateAt: expect.any(String),
          });
        });
      });
  });

  it('Mutation RemoveJob', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: REMOVE_JOB,
        variables: {
          id: instanceVariable.jobId,
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.RemoveJob).toStrictEqual(1);
      });
  });

  it('Query JobTrend', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_JOB_TREND,
        variables: {
          endYear: '2022',
          geo: 'TH',
          startYear: '2021',
        },
      })
      .expect(200)
      .expect((res) => {
        res.body.data.JobTrend.averages.forEach((element) => {
          expect(element).toMatchObject({
            name: expect.any(String),
            score: expect.any(Number),
          });
        });
        res.body.data.JobTrend.keyword.forEach((element) => {
          expect(element).toStrictEqual(expect.any(String));
        });
        res.body.data.JobTrend.timelineData.forEach((element) => {
          expect(element).toMatchObject({
            formattedAxisTime: expect.any(String),
            formattedTime: expect.any(String),
            formattedValue: expect.arrayContaining([expect.any(String)]),
            hasData: expect.arrayContaining([expect.any(Boolean)]),
            time: expect.any(String),
            value: expect.arrayContaining([expect.any(Number)]),
          });
        });
        expect(res.body.data.JobTrend).toMatchObject({
          endDate: expect.any(String),
          startDate: expect.any(String),
        });
      });
  });
});

describe('PublisherResolver GraphQL', () => {
  let app: INestApplication;
  let companyService: CompanyService;
  let publisherService: PublisherService;
  let publisherResolver: PublisherResolver;
  const instanceVariable = {
    companyId: '',
    publisherId: '',
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, CompanyModule, PublisherModule],
      providers: [PublisherResolver],
    })
      .overrideGuard(JwtAuthGuard)
      .useValue({ canActivate: () => true })
      .compile();
    companyService = moduleFixture.get<CompanyService>(CompanyService);
    publisherService = moduleFixture.get<PublisherService>(PublisherService);
    publisherResolver = moduleFixture.get<PublisherResolver>(PublisherResolver);
    app = moduleFixture.createNestApplication();

    await app.init();
    const mockCompanyInput = {
      description: 'integration_test',
      industries: ['Tech'],
      locations: ['Singapore'],
      name: 'integration_test',
      size: CompanySizeEnum.large,
      logoImage: 'https://integration_test.png',
    };
    const testCompany = await companyService.createCompany(mockCompanyInput);
    instanceVariable.companyId = testCompany._id;
  });

  afterAll(async () => {
    const companyDeleteCount = await companyService.removeCompany(
      instanceVariable.companyId,
    );
    expect(companyDeleteCount).toStrictEqual(1);
    const publisherDeleteCount = await publisherService.removePublisher(
      instanceVariable.publisherId,
    );
    expect(publisherDeleteCount).toStrictEqual(1);
    await app.close();
  });

  it('Mutation CreatePublisher', async () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: CREATE_PUBLISHER,
        variables: {
          PublisherRegisterInputDto: {
            companyId: instanceVariable.companyId,
            email: 'integration_test@gmail.com',
            firstname: 'integration_test',
            job: 'integration_test',
            lastname: 'integration_test',
            password: '123qweasd',
            phonenumber: '0899999999',
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        instanceVariable.publisherId = res.body.data.CreatePublisher._id;
        expect(res.body.data.CreatePublisher).toMatchObject({
          _id: instanceVariable.publisherId,
          companyId: instanceVariable.companyId,
          createAt: expect.any(String),
          email: 'integration_test@gmail.com',
          firstname: 'integration_test',
          job: 'integration_test',
          lastname: 'integration_test',
          middlename: null,
          phonenumber: '0899999999',
          role: UserRoleEnum.publisher,
          status: ApproveStatusEnum.pending,
          updateAt: expect.any(String),
        });
      });
  });

  it('Query GetPublisherBySession', async () => {
    const mockReq: any = {
      session: {
        user: {
          _id: instanceVariable.publisherId,
        },
      },
    };
    const res = await publisherResolver.GetPublisherBySession(mockReq);
    expect(JSON.parse(JSON.stringify(res))).toMatchObject({
      _id: instanceVariable.publisherId,
      companyId: instanceVariable.companyId,
      createAt: expect.any(String),
      email: 'integration_test@gmail.com',
      firstname: 'integration_test',
      job: 'integration_test',
      lastname: 'integration_test',
      phonenumber: '0899999999',
      role: UserRoleEnum.publisher,
      status: ApproveStatusEnum.pending,
      updateAt: expect.any(String),
    });
    return;
  });

  it('Mutation UpdatePublisher', async () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: UPDATE_PUBLISHER,
        variables: {
          PublisherUpdateInputDto: {
            id: instanceVariable.publisherId,
            status: ApproveStatusEnum.approved,
            email: 'integration_test_upddated@gmail.com',
            firstname: 'integration_test_updated',
            job: 'integration_test_updated',
            lastname: 'integration_test_updated',
            password: '123qweasdzxc',
            phonenumber: '0888888888',
          },
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.UpdatePublisher).toMatchObject({
          _id: instanceVariable.publisherId,
          companyId: instanceVariable.companyId,
          createAt: expect.any(String),
          email: 'integration_test_upddated@gmail.com',
          firstname: 'integration_test_updated',
          job: 'integration_test_updated',
          lastname: 'integration_test_updated',
          middlename: null,
          phonenumber: '0888888888',
          role: UserRoleEnum.publisher,
          status: ApproveStatusEnum.approved,
          updateAt: expect.any(String),
        });
      });
  });

  it('Query Publisher', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_PUBLISHER,
        variables: {
          id: instanceVariable.publisherId,
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.Publisher.password).toBeUndefined();
        expect(res.body.data.Publisher).toMatchObject({
          _id: instanceVariable.publisherId,
          companyId: instanceVariable.companyId,
          createAt: expect.any(String),
          email: 'integration_test_upddated@gmail.com',
          firstname: 'integration_test_updated',
          job: 'integration_test_updated',
          lastname: 'integration_test_updated',
          middlename: null,
          phonenumber: '0888888888',
          role: UserRoleEnum.publisher,
          status: ApproveStatusEnum.approved,
          updateAt: expect.any(String),
        });
      });
  });

  it('Query Publishers', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_PUBLISHERS,
        variables: {
          limit: 3,
          skip: 0,
          sortBy: 'createAt',
          sortOrder: 'desc',
          status: 'approved',
        },
      })
      .expect(200)
      .expect(async (res) => {
        expect(res.body.data.Publishers.totalCount).toStrictEqual(
          expect.any(Number),
        );
        res.body.data.Publishers.publisherResult.forEach((element) => {
          expect(element.password).toBeUndefined();
          expect(element).toMatchObject<Partial<PublisherWithId>>({
            _id: expect.any(String),
            companyId: expect.any(String),
            createAt: expect.any(String),
            email: expect.any(String),
            firstname: expect.any(String),
            job: expect.any(String),
            lastname: expect.any(String),
            phonenumber: expect.any(String),
            role: expect.any(String),
            status: expect.any(String),
            updateAt: expect.any(String),
          });
        });
      });
  });
});

describe('ReviewResolver GraphQL', () => {
  let app: INestApplication;
  let reviewService: ReviewService;
  let companyService: CompanyService;
  const instanceVariable = {
    reviewId: '',
    companyId: '',
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, ReviewModule, CompanyModule],
    }).compile();
    reviewService = moduleFixture.get<ReviewService>(ReviewService);
    companyService = moduleFixture.get<CompanyService>(CompanyService);
    app = moduleFixture.createNestApplication();

    await app.init();
    const mockCompanyInput = {
      description: 'integration_test',
      industries: ['Tech'],
      locations: ['Singapore'],
      name: 'integration_test',
      size: CompanySizeEnum.large,
      logoImage: 'https://integration_test.png',
    };
    const testCompany = await companyService.createCompany(mockCompanyInput);
    instanceVariable.companyId = testCompany._id;

    const mockNewReview = {
      companyId: instanceVariable.companyId,
      title: 'integration_test',
      authorPosition: 'integration_test',
      content: 'integration_test',
      pro: 'integration_test',
      con: 'integration_test',
    };
    const testReview = await reviewService.createReview(mockNewReview);
    instanceVariable.reviewId = testReview._id;
  });

  afterAll(async () => {
    const deleteCount = await companyService.removeCompany(
      instanceVariable.companyId,
    );
    expect(deleteCount).toStrictEqual(1);
    await app.close();
  });

  // * Review Module
  it('Query Review', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_REVIEW,
        variables: {
          id: instanceVariable.reviewId,
        },
      })
      .expect(200)
      .expect((res) =>
        expect(res.body.data.Review).toMatchObject({
          _id: instanceVariable.reviewId,
          authorPosition: 'integration_test',
          companyId: instanceVariable.companyId,
          content: 'integration_test',
          createAt: expect.any(String),
          score: null,
          pro: 'integration_test',
          con: 'integration_test',
          sentiment: null,
          title: 'integration_test',
          topic: null,
          updateAt: expect.any(String),
        }),
      );
  });

  it('Query Reviews', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_REVIEWS,
        variavles: {
          companyId: '6204e876816570ee3a495e66',
          limit: 5,
          sentiment: 'negative',
          skip: 0,
          sortBy: 'createAt',
          sortOrder: 'desc',
          topic: 'Time',
        },
      })
      .expect((res) => {
        expect(res.body.data.Reviews.totalCount).toStrictEqual(
          expect.any(Number),
        );
        res.body.data.Reviews.reviewResult.forEach((element) => {
          expect(element).toMatchObject<Partial<ReviewWithId>>({
            _id: expect.any(String),
            authorPosition: expect.any(String),
            companyId: expect.any(String),
            content: expect.any(String),
            createAt: expect.any(String),
            score: expect.any(Number),
            sentiment: expect.any(String),
            title: expect.any(String),
            topic: expect.any(String),
            updateAt: expect.any(String),
          });
        });
      });
  });

  it('Mutation RemoveReview', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: REMOVE_REVIEW,
        variables: {
          id: instanceVariable.reviewId,
        },
      })
      .expect(200)
      .expect((res) => {
        expect(res.body.data.RemoveReview).toStrictEqual(1);
      });
  });
});

describe('WatchingListResolver GraphQL', () => {
  let app: INestApplication;
  let userService: UserService;
  let companyService: CompanyService;
  let watchingListService: WatchingListService;
  const newDob = new Date();
  const instanceVariable = {
    userId: '',
    companyId: '',
    companyName: '',
    watchingListId: '',
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, UserModule, CompanyModule, WatchingListModule],
    })
      .overrideGuard(JwtAuthGuard)
      .useValue({ canActivate: () => true })
      .compile();
    userService = moduleFixture.get<UserService>(UserService);
    companyService = moduleFixture.get<CompanyService>(CompanyService);
    watchingListService =
      moduleFixture.get<WatchingListService>(WatchingListService);
    app = moduleFixture.createNestApplication();

    await app.init();
    // create test user
    const mockUserInput = {
      address: '165/76 Bangkok',
      email: 'integration_test@gmail.com',
      firstname: 'integration_test',
      middlename: 'integration_test',
      lastname: 'integration_test',
      password: 'integration_test',
      phonenumber: '0998885889',
      dob: newDob,
      description: 'integration_test',
    };
    const testUser = await userService.createUser(mockUserInput);
    instanceVariable.userId = testUser._id;

    // create test company
    const mockCompanyInput = {
      description: 'integration_test',
      industries: ['Tech'],
      locations: ['Singapore'],
      name: 'integration_test',
      size: CompanySizeEnum.large,
      logoImage: 'https://integration_test.png',
    };
    const testCompany = await companyService.createCompany(mockCompanyInput);
    instanceVariable.companyId = testCompany._id;
    instanceVariable.companyName = testCompany.name;
  });

  afterAll(async () => {
    // remove test user
    const userDeleteCount = await userService.removeUser(
      instanceVariable.userId,
    );
    expect(userDeleteCount).toStrictEqual(1);

    // remove test company
    const companyDeleteCount = await companyService.removeCompany(
      instanceVariable.companyId,
    );
    expect(companyDeleteCount).toStrictEqual(1);

    // remove test watchinglist
    const watchingListDeleteCount =
      await watchingListService.deleteWatchingList(
        instanceVariable.watchingListId,
      );
    expect(watchingListDeleteCount).toStrictEqual(1);
    await app.close();
  });

  it('Mutation CreateWatchingList', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: CREATE_WATCHINGLIST,
        variables: {
          WatchingListInputDto: {
            userId: instanceVariable.userId,
            name: instanceVariable.companyName,
            location: 'Seattle, WA',
            listType: ListTypeEnum.company,
            companyId: instanceVariable.companyId,
          },
        },
      })
      .expect(200)
      .expect((res) => {
        instanceVariable.watchingListId = res.body.data.CreateWatchingList._id;
        expect(
          res.body.data.CreateWatchingList,
        ).toMatchObject<WatchingListWithId>({
          _id: instanceVariable.watchingListId,
          list: [
            {
              name: instanceVariable.companyName,
              companyId: instanceVariable.companyId,
              location: 'Seattle, WA',
              listType: ListTypeEnum.company,
              createAt: expect.any(String),
            },
          ],
          userId: instanceVariable.userId,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });

  it('Query WatchingList', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_WATCHINGLIST,
        variables: {
          id: instanceVariable.watchingListId,
        },
      })
      .expect((res) => {
        res.body.data.WatchingList.list.forEach((element) => {
          expect(element).toMatchObject({
            companyId: expect.any(String),
            createAt: expect.any(String),
            listType: expect.any(String),
            location: expect.any(String),
            name: expect.any(String),
          });
        });
        expect(res.body.data.WatchingList).toMatchObject<
          Partial<WatchingListWithId>
        >({
          _id: expect.any(String),
          createAt: expect.any(String),
          updateAt: expect.any(String),
          userId: expect.any(String),
        });
      });
  });

  it('Query WatchingLists', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_WATCHINGLISTS,
        variables: {
          limit: 2,
          skip: 0,
          sortBy: 'createAt',
          sortOrder: 'desc',
        },
      })
      .expect((res) => {
        res.body.data.WatchingLists.forEach((element) => {
          element.list.forEach((element) => {
            expect(element).toMatchObject({
              companyId: expect.any(String),
              createAt: expect.any(String),
              listType: expect.any(String),
              location: expect.any(String),
              name: expect.any(String),
            });
          });
          expect(element).toMatchObject<Partial<WatchingListWithId>>({
            _id: expect.any(String),
            createAt: expect.any(String),
            updateAt: expect.any(String),
            userId: expect.any(String),
          });
        });
      });
  });

  it('Mutation RemoveWatchingList', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: REMOVE_WATCHINGLIST,
        variables: {
          WatchingListInputDto: {
            userId: instanceVariable.userId,
            name: instanceVariable.companyName,
            location: 'Seattle, WA',
            listType: ListTypeEnum.company,
            companyId: instanceVariable.companyId,
          },
        },
      })
      .expect(200)
      .expect((res) => {
        expect(
          res.body.data.RemoveWatchingList,
        ).toMatchObject<WatchingListWithId>({
          _id: instanceVariable.watchingListId,
          list: [],
          userId: instanceVariable.userId,
          createAt: expect.any(String),
          updateAt: expect.any(String),
        });
      });
  });
});

describe('LocationResolver GraphQL', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  // * Location Module
  it('Query Location', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_LOCATION,
        variables: {
          id: '621c6c8da2d9ac5a1bd108bc',
        },
      })
      .expect((res) =>
        expect(res.body.data.Location).toMatchObject({
          _id: expect.any(String),
          name: expect.any(String),
        }),
      );
  });

  it('Query Locations', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_LOCATIONS,
      })
      .expect((res) => {
        res.body.data.Locations.forEach((element) => {
          expect(element).toMatchObject<LocaitionWithId>({
            _id: expect.any(String),
            name: expect.any(String),
          });
        });
      });
  });
});

describe('DashboardResolver GraphQL', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideGuard(JwtAuthGuard)
      .useValue({ canActivate: () => true })
      .compile();
    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  // * Dashboard Module
  it('Query Dashboard', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_DASHBOARDSTAT,
      })
      .expect(200)
      .expect((res) => {
        res.body.data.DashboardStat.activeData.forEach((element) => {
          expect(element).toMatchObject<ActiveDataType>({
            name: expect.any(String),
            totalCount: expect.any(Number),
          });
        });
        res.body.data.DashboardStat.nonActiveData.forEach((element) => {
          expect(element).toMatchObject<NonActiveDataType>({
            name: expect.any(String),
            totalCount: expect.any(Number),
          });
        });
      });
  });

  it('Query LastestJobStat', () => {
    return request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: GET_LASTESTJOBSTAT,
      })
      .expect(200)
      .expect((res) => {
        res.body.data.LastestJobStat.forEach((element) => {
          expect(element).toMatchObject<LastestJobDto>({
            name: expect.any(String),
            count: expect.any(Number),
          });
        });
      });
  });
});
