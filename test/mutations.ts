export const CREATE_USER = `
mutation CreateUser($UserRegisterInputDto: UserRegisterInputDto!) {
    CreateUser (UserRegisterInputDto: $UserRegisterInputDto) {
      _id
      email
      firstname
      lastname
      middlename
      dob
      phonenumber
      address
      role
      education {
        school
        graduationYear
        educationLevel
      }
      experience {
        description
        name
        year
      }
      skill {
        name
      }
      description
    }
  }
`;

export const UPDATE_USER = `
mutation UpdateUser($UserUpdateInputDto: UserUpdateInputDto!) {
    UpdateUser(UserUpdateInputDto: $UserUpdateInputDto) {
        _id
        email
        firstname
        lastname
        middlename
        dob
        phonenumber
        address
        role
        education {
          school
          graduationYear
          educationLevel
        }
        experience {
          description
          name
          year
        }
        skill {
          name
        }
        description
    }
  }
`;

export const LOGIN = `
mutation Login($UserLoginInputDTO: UserLoginInputDTO!) {
    Login (UserLoginInputDTO: $UserLoginInputDTO){
      _id
      email
      firstname
      middlename
      lastname
      dob
      phonenumber
      address
      role
    }
  }
`;

export const CREATE_COMPANY = `
mutation CreateCompany($CreateCompanyInputDto: CreateCompanyInputDto!){
  CreateCompany(CreateCompanyInputDto: $CreateCompanyInputDto) {
    _id
    createAt
    description
    industries
    locations
    logoImage
    name
    properties {
      name
      negative
      positive
    }
    shortName
    size
    sourceId
    sourceType
    status
    updateAt
  }
}`;

export const UPDATE_COMPANY = `
mutation UpdateCompany($UpdateCompanyInputDto: UpdateCompanyInputDto!){
  UpdateCompany(UpdateCompanyInputDto: $UpdateCompanyInputDto) {
    _id
    createAt
    description
    industries
    locations
    logoImage
    name
    properties {
      name
      negative
      positive
    }
    shortName
    size
    sourceId
    sourceType
    status
    updateAt
  }
}
`;

export const REMOVE_COMPANY = `
mutation RemoveCompany($id: String!){
  RemoveCompany(id: $id)
}
`;

export const CREATE_JOB = `
mutation CreateJob($CreateJobInputDto: CreateJobInputDto!) {
  CreateJob(CreateJobInputDto: $CreateJobInputDto) {
  	_id
    applyUrl
    benefits
    categories
    companyId
    companyName
    contents
    createAt
    experienceLevels
    jobType
    locations
    name
    description
    educationLevel
    requirements
    remote
    salary
    shortName
    sourceId
    sourceType
    status
    updateAt
  }
}
`;

export const UPDATE_JOB = `
mutation UpdateJob($UpdateJobInputDto: UpdateJobInputDto!) {
  UpdateJob(UpdateJobInputDto: $UpdateJobInputDto) {
  	_id
    applyUrl
    benefits
    categories
    companyId
    companyName
    contents
    createAt
    experienceLevels
    jobType
    locations
    requirements
    description
    educationLevel
    name
    remote
    salary
    shortName
    sourceId
    sourceType
    status
    updateAt
  }
}
`;

export const REMOVE_JOB = `
mutation RemoveJob($id: String!){
  RemoveJob(id: $id)
}
`;

export const CREATE_PUBLISHER = `
mutation CreatePublisher($PublisherRegisterInputDto: PublisherRegisterInputDto!){
  CreatePublisher(PublisherRegisterInputDto: $PublisherRegisterInputDto) {
    _id
    email
    password
    firstname
    middlename
    lastname
    phonenumber
    job
    companyId
    status
    role
    createAt
    updateAt
  }
}
`;

export const UPDATE_PUBLISHER = `
mutation UpdatePublisher($PublisherUpdateInputDto: PublisherUpdateInputDto!) {
  UpdatePublisher(PublisherUpdateInputDto: $PublisherUpdateInputDto) {
    _id
    email
    password
    firstname
    middlename
    lastname
    phonenumber
    job
    companyId
    status
    role
    createAt
    updateAt
  }
}
`;

export const REMOVE_REVIEW = `
mutation RemoveReview($id: String!){
  RemoveReview(id: $id)
}
`;

export const CREATE_FAVORITE = `
mutation CreateFavorite($FavoriteJobInputDto: FavoriteJobInputDto!){
  CreateFavorite(FavoriteJobInputDto: $FavoriteJobInputDto){
    _id
    jobsId
    userId
    createAt
   	updateAt
  }
}
`;

export const REMOVE_FAVORITE = `
mutation RemoveFavorite($FavoriteJobInputDto: FavoriteJobInputDto!) {
  RemoveFavorite(FavoriteJobInputDto: $FavoriteJobInputDto) {
    _id
    jobsId
    userId
    createAt
   	updateAt
  }
}
`;

export const CREATE_WATCHINGLIST = `
mutation CreateWatchingList($WatchingListInputDto: WatchingListInputDto!){
  CreateWatchingList(WatchingListInputDto: $WatchingListInputDto){
    _id
    list{
      name
      listType
      companyId
      createAt
      location
    }
    createAt
    updateAt
    userId
  }
}
`;

export const REMOVE_WATCHINGLIST = `
mutation RemoveWatchingList($WatchingListInputDto: WatchingListInputDto!) {
  RemoveWatchingList(WatchingListInputDto: $WatchingListInputDto) {
    _id
    list{
      name
      listType
      companyId
      createAt
      location
    }
    createAt
    updateAt
    userId
  }
}
`;
