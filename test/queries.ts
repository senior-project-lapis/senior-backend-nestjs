// import { gql } from 'apollo-server-express';
// import { print } from 'graphql';

export const GET_SERVER_STATUS = `
  {
    ServerStatus {
      status
    }
  }
`;

export const GET_COMPANIES = `
  query Companies(
    $industry: String
    $limit: Int = 1000
    $location: String
    $name: String
    $size: String
    $skip: Int = 0
    $sortBy: String
    $sortOrder: String
  ) {
    Companies(
      industry: $industry
      limit: $limit
      location: $location
      name: $name
      size: $size
      skip: $skip
      sortBy: $sortBy
      sortOrder: $sortOrder
    ) {
      totalCount
      companyResult {
        _id
        createAt
        description
        industries
        locations
        logoImage
        name
        properties {
          name
          negative
          positive
        }
        shortName
        size
        sourceId
        sourceType
        status
        updateAt
      }
    }
  }
`;

export const GET_COMPANY = `
  query Company($id: String!) {
    Company(id: $id) {
      _id
      createAt
      description
      industries
      locations
      logoImage
      name
      properties {
        name
        negative
        positive
      }
      shortName
      size
      sourceId
      sourceType
      status
      updateAt
    }
  }
`;

export const GET_COUNTRIES = `
  {
    Countries {
      code
      name
    }
  }
`;

export const GET_FAVORITE = `
  query Favorite($id: String!) {
    Favorite(id: $id) {
      _id
      createAt
      jobsId
      updateAt
      userId
    }
  }
`;

export const GET_FAVORITES = `
  query Favorites($userId: String!) {
    Favorites(userId: $userId) {
      _id
      applyUrl
      benefits
      categories
      companyId
      companyName
      contents
      createAt
      description
      educationLevel
      experienceLevels
      jobType
      locations
      name
      remote
      requirements
      salary
      shortName
      sourceId
      sourceType
      status
      updateAt
    }
  }
`;

export const GET_JOB = `
  query Job($id: String!) {
    Job(id: $id) {
      _id
      applyUrl
      benefits
      categories
      companyId
      description
      educationLevel
      requirements
      companyName
      contents
      createAt
      experienceLevels
      jobType
      locations
      name
      remote
      salary
      shortName
      sourceId
      sourceType
      status
      updateAt
    }
  }
`;

export const GET_JOBS = `
  query Jobs(
    $category: String
    $companyId: String
    $limit: Int = 1000
    $location: String
    $name: String
    $remote: Boolean
    $salary: Int
    $skip: Int = 0
    $jobType: String
    $sortBy: String
    $sortOrder: String
    $status: String
    $since: DateTime
  ) {
    Jobs(
      category: $category
      companyId: $companyId
      limit: $limit
      location: $location
      name: $name
      remote: $remote
      salary: $salary
      skip: $skip
      jobType: $jobType
      sortBy: $sortBy
      sortOrder: $sortOrder
      status: $status
      since: $since
    ) {
      totalCount
      jobResult {
        _id
        applyUrl
        benefits
        categories
        companyId
        companyName
        contents
        createAt
        experienceLevels
        jobType
        locations
        name
        remote
        salary
        shortName
        sourceId
        sourceType
        status
        updateAt
      }
    }
  }
`;

export const GET_JOB_TREND = `
  query JobTrend($endYear: String!, $geo: String, $startYear: String!) {
    JobTrend(endYear: $endYear, geo: $geo, startYear: $startYear) {
      averages {
        name
        score
      }
      endDate
      keyword
      startDate
      timelineData {
        formattedAxisTime
        formattedTime
        formattedValue
        hasData
        time
        value
      }
    }
  }
`;

export const GET_PUBLISHER = `
  query Publisher($id: String!) {
    Publisher(id: $id) {
      _id
      companyId
      createAt
      email
      firstname
      job
      lastname
      middlename
      phonenumber
      role
      status
      updateAt
    }
  }
`;

export const GET_PUBLISHERS = `
  query Publishers(
    $limit: Int = 1000
    $skip: Int = 0
    $sortBy: String
    $sortOrder: String
    $status: String
  ) {
    Publishers(
      limit: $limit
      skip: $skip
      sortBy: $sortBy
      sortOrder: $sortOrder
      status: $status
    ) {
      totalCount
      publisherResult {
        _id
        companyId
        createAt
        email
        firstname
        job
        lastname
        middlename
        phonenumber
        role
        status
        updateAt
      }
    }
  }
`;

export const GET_REVIEW = `
  query Review($id: String!) {
    Review(id: $id) {
      _id
      authorPosition
      companyId
      con
      content
      createAt
      pro
      score
      sentiment
      title
      topic
      updateAt
    }
  }
`;

export const GET_REVIEWS = `
  query Reviews(
    $companyId: String
    $limit: Int = 1000
    $sentiment: String
    $skip: Int = 0
    $sortBy: String
    $sortOrder: String
    $topic: String
  ) {
    Reviews(
      companyId: $companyId
      limit: $limit
      sentiment: $sentiment
      skip: $skip
      sortBy: $sortBy
      sortOrder: $sortOrder
      topic: $topic
    ) {
      totalCount
      reviewResult {
        _id
        authorPosition
        companyId
        con
        content
        createAt
        pro
        score
        sentiment
        title
        topic
        updateAt
      }
    }
  }
`;

export const GET_USER = `
  query Users($id: String!) {
    User(id: $id) {
      _id
      address
      createAt
      dob
      education {
        educationLevel
        graduationYear
        school
      }
      email
      experience {
        description
        name
        year
      }
      firstname
      lastname
      middlename
      phonenumber
      role
      skill {
        name
      }
      updateAt
    }
  }
`;

export const GET_USERS = `
  query Users($limit: Int = 1000, $skip: Int = 0) {
    Users(limit: $limit, skip: $skip) {
      _id
      address
      createAt
      dob
      education {
        educationLevel
        graduationYear
        school
      }
      email
      experience {
        description
        name
        year
      }
      firstname
      lastname
      middlename
      phonenumber
      role
      skill {
        name
      }
      updateAt
    }
  }
`;

export const GET_WATCHINGLIST = `
  query WatchingList($id: String!) {
    WatchingList(id: $id) {
      _id
      createAt
      list {
        companyId
        createAt
        listType
        location
        name
      }
      updateAt
      userId
    }
  }
`;

export const GET_WATCHINGLISTS = `
  query WatchingLists(
    $limit: Int = 1000
    $skip: Int = 0
    $sortBy: String
    $sortOrder: String
  ) {
    WatchingLists(
      limit: $limit
      skip: $skip
      sortBy: $sortBy
      sortOrder: $sortOrder
    ) {
      _id
      createAt
      list {
        companyId
        createAt
        listType
        location
        name
      }
      updateAt
      userId
    }
  }
`;

export const GET_LOCATION = `
  query Location($id: String!) {
    Location(id: $id) {
      _id
      name
    }
  }
`;

export const GET_LOCATIONS = `
  {
    Locations {
      _id
      name
    }
  }
`;

export const GET_DASHBOARDSTAT = `
  {
    DashboardStat {
      activeData {
        name
        totalCount
      }
      nonActiveData {
        name
        totalCount
      }
    }
  }
`;

export const GET_LASTESTJOBSTAT = `
  {
    LastestJobStat {
      name
      count
    }
  }
`;

export const GET_USERBYSESSION = `
query GetUserBySession{
  GetUserBySession {
    _id
    email
    firstname
    lastname
    middlename
    dob
    phonenumber
    address
    role
    education {
      school
      graduationYear
      educationLevel
    }
    experience {
      description
      name
      year
    }
    skill {
      name
    }
    description
  }
}
`;
