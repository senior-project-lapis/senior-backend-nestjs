import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthenticationModule } from '../authentication/authentication.module';
import { CompanyModule } from '../company/company.module';
import { ConfigModule } from '../config/config.module';
import { JobModule } from '../job/job.module';
import { UserModule } from '../user/user.module';
import watchingListSchema from './entity/watching-list.entity';
import { WatchingListResolver } from './watching-list.resolver';
import { WatchingListService } from './watching-list.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'watchinglists',
        schema: watchingListSchema,
      },
    ]),
    forwardRef(() => AuthenticationModule),
    forwardRef(() => UserModule),
    CompanyModule,
    ConfigModule,
  ],
  providers: [WatchingListService, WatchingListResolver],
  exports: [WatchingListService, ConfigModule],
})
export class WatchingListModule {}
