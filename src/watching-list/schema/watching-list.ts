import { WatchingListType } from '../@types/watching-list.type';

export type WatchingListSchema = {
  _id: string;

  list: WatchingListType[];
  userId: string;

  createAt: Date;
  updateAt: Date;
};
