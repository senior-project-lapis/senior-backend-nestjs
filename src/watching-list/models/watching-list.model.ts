/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType } from '@nestjs/graphql';
import { CompanyWatchingList } from '../@types/watching-list-company.type';
import { JobWatchingList } from '../@types/watching-list-job.type';
import { WatchingListType } from '../@types/watching-list.type';
import { IWatchingList } from '../interfaces/watching-list';

@ObjectType()
export class WatchingList implements IWatchingList {
  @Field(() => [WatchingListType])
  list: WatchingListType[];

  @Field(() => String)
  userId: string;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class WatchingListWithId extends WatchingList {
  @Field(() => String)
  _id: string;
}
