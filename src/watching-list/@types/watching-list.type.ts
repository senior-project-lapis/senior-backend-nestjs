/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, InputType, Int, ObjectType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ListTypeEnum } from '../schema/enum/list-type.enum';

@ObjectType()
export class WatchingListType {
  @Field(() => String)
  name: string;

  @Field(() => String, { nullable: true })
  companyId?: string;

  @Field(() => String)
  location: string;

  @Field(() => String)
  listType: ListTypeEnum;

  @Field(() => String)
  createAt: Date;
}
