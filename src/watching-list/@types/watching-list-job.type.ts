/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, InputType, Int, ObjectType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@ObjectType()
export class JobWatchingList {
  @Field(() => String)
  name: string;

  @Field(() => String)
  location: string;

  @Field(() => String)
  createAt: Date;
}
