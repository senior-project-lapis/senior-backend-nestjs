import { Document, Schema } from 'mongoose';
import { ListTypeEnum } from '../schema/enum/list-type.enum';
import { WatchingListSchema } from '../schema/watching-list';

export const ListTypeConstant: ListTypeEnum[] = [
  ListTypeEnum.job,
  ListTypeEnum.company,
];

const watchingListSchema = new Schema<WatchingListDocument>(
  {
    list: {
      type: [
        {
          name: String,
          location: String,
          companyId: { type: String, required: true },
          listType: { type: String, enum: ListTypeConstant },
          createAt: { type: Date, default: Date.now },
        },
      ],
      required: true,
    },
    userId: { type: String, required: true },

    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type WatchingListDocument = Document & WatchingListSchema;

export default watchingListSchema;
