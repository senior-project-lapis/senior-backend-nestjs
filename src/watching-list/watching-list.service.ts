import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import nodemailer from 'nodemailer';
import { Model } from 'mongoose';
import fs from 'fs';
import handlebars from 'handlebars';
import { WatchingListDocument } from './entity/watching-list.entity';
import { WatchingListNotExistsException } from '../common/exception/watching-list.not-exists.exception';
import { WatchingListInputDto } from './dtos/inputs/create-watching-list.input.dto';
import { DuplcatedWatchingListException } from '../common/exception/duplicated-watching-list.exception';
import { promisify } from 'util';
import { CreateJobInputDto } from '../job/dtos/inputs/create-job.input.dto';
import { UserDocument } from '../user/entity/user.entity';
import { JobService } from '../job/job.service';
import { WatchingListArgs } from './dtos/args/watching-list.args.dto';
import { WatchingList, WatchingListWithId } from './models/watching-list.model';
import { UserService } from '../user/user.service';
import { ConfigService } from '../config/config.service';

@Injectable()
export class WatchingListService {
  constructor(
    @InjectModel('watchinglists')
    private readonly watchingListModel: Model<WatchingListDocument>,

    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,

    private readonly configService: ConfigService,
  ) {}

  async getWatchingListsByUserId(
    userId: string,
  ): Promise<WatchingListDocument> {
    return this.watchingListModel.findOne({ userId }).lean();
  }

  async getWatchingLists(
    watchingListArgs: WatchingListArgs,
  ): Promise<WatchingListDocument> {
    const { userId, companyId, jobName, sortBy, sortOrder, skip, limit } =
      watchingListArgs;
    const query = this.watchingListModel.find();

    if (jobName) {
      const jobNameRegex = new RegExp(jobName, 'i');
      query.where('list.name', jobNameRegex);
    }
    if (companyId) {
      query.where('list.companyId', companyId);
    }
    if (userId) {
      query.where('userId', userId);
    }

    return query
      .sort([[sortBy, sortOrder]])
      .skip(skip)
      .limit(limit)
      .lean();
  }

  async getById(id: string): Promise<WatchingListDocument> {
    return this.watchingListModel.findById(id).lean();
  }

  async createWatchingList(
    payload: WatchingListInputDto,
  ): Promise<WatchingListDocument> {
    const { userId, name, listType, location, companyId } = payload;
    const isExistInWatchingList = await this.watchingListModel.exists({
      userId,
      'list.name': name,
    });
    if (isExistInWatchingList) throw new DuplcatedWatchingListException();

    const watchingList = await this.watchingListModel.findOneAndUpdate(
      { userId },
      {
        $push: {
          list: {
            name,
            listType,
            companyId: companyId,
            location,
          },
        },
        updateAt: new Date(),
      },
      { new: true, upsert: true },
    );
    if (!watchingList) throw new WatchingListNotExistsException();
    return watchingList;
  }

  async removeWatchingList(
    payload: WatchingListInputDto,
  ): Promise<WatchingListDocument> {
    const { userId, name, listType, location, companyId } = payload;
    const watchingList = await this.watchingListModel.findOneAndUpdate(
      { userId },
      { $pull: { list: { name, listType, location, companyId } } },
      { new: true },
    );
    if (!watchingList) throw new WatchingListNotExistsException();
    return watchingList;
  }

  async sendAlert(jobPayload: CreateJobInputDto): Promise<void> {
    const { name, companyName, companyId } = jobPayload;
    const jobNameRegex = new RegExp(name, 'i');

    // Get user in watchingList
    const watchingLists = await this.watchingListModel
      .find()
      .or([{ 'list.companyId': companyId }, { 'list.name': jobNameRegex }])
      .lean();

    // Read template and compile
    const readFile = promisify(fs.readFile);
    const html: string = await readFile(
      './src/watching-list/templates/email-template.html',
      'utf8',
    );
    const template = handlebars.compile(html);

    // Create transportor
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: this.configService.get().mailer.user,
        pass: this.configService.get().mailer.password,
      },
    });

    // Iterate to send email
    watchingLists.forEach(async (element) => {
      const { userId } = element;
      const user = await this.userService.getById(userId);
      if (user) {
        // Prepare variable to map with html template
        const replacements = {
          firstname: user.firstname,
          jobName: name,
          companyName,
        };

        // Setting mail option
        const mailOptions = {
          from: 'lapis.mailer@gmail.com',
          to: user.email,
          subject: 'Lapis new job notification',
          html: template(replacements),
        };

        const response = await transporter.sendMail(mailOptions);
        if (response.envelope.to.length !== 0) {
          Logger.log(`Send email to ${user.email} complete.`);
        }
      }
    });
  }

  async deleteWatchingList(id: string): Promise<number> {
    const { deletedCount } = await this.watchingListModel.deleteOne({
      _id: id,
    });
    return deletedCount;
  }
}
