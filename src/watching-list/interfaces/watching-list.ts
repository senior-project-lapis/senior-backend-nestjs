import { WatchingListType } from '../@types/watching-list.type';

export interface IWatchingList {
  list: WatchingListType[];
  userId: string;

  createAt: Date;
  updateAt: Date;
}
