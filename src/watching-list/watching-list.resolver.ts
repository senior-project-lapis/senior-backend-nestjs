import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import { WatchingListArgs } from './dtos/args/watching-list.args.dto';
import { WatchingListInputDto } from './dtos/inputs/create-watching-list.input.dto';
import { WatchingList, WatchingListWithId } from './models/watching-list.model';
import { WatchingListService } from './watching-list.service';

@Resolver(() => WatchingList)
export class WatchingListResolver {
  constructor(private readonly watchingListService: WatchingListService) {}

  // watching list
  @Query(() => WatchingListWithId, { name: 'WatchingList', nullable: true })
  async GetWatchingList(@Args('id', { type: () => String }) id: string) {
    return this.watchingListService.getById(id);
  }

  // watching lists
  @Query(() => [WatchingListWithId], { name: 'WatchingLists' })
  async GetWatchingLists(@Args() watchingListArgs: WatchingListArgs) {
    return this.watchingListService.getWatchingLists(watchingListArgs);
  }

  // @UseGuards(JwtAuthGuard)
  @Mutation(() => WatchingListWithId)
  async CreateWatchingList(
    @Args('WatchingListInputDto')
    createWatchingListInputDto: WatchingListInputDto,
  ) {
    return this.watchingListService.createWatchingList(
      createWatchingListInputDto,
    );
  }

  // @UseGuards(JwtAuthGuard)
  @Mutation(() => WatchingListWithId)
  async RemoveWatchingList(
    @Args('WatchingListInputDto')
    removeWatchingListInputDto: WatchingListInputDto,
  ) {
    return this.watchingListService.removeWatchingList(
      removeWatchingListInputDto,
    );
  }
}
