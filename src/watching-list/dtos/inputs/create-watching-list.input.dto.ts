import { Field, InputType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';
import { WatchingList } from '../../models/watching-list.model';
import { ListTypeEnum } from '../../schema/enum/list-type.enum';

@InputType()
export class WatchingListInputDto implements Pick<WatchingList, 'userId'> {
  @Field(() => String)
  listType: ListTypeEnum;

  @Field(() => String, { nullable: true })
  @IsOptional()
  companyId?: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  location: string;

  @Field(() => String)
  userId: string;
}
