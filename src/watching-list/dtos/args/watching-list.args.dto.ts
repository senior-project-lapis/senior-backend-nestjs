/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';
import { SkipLimitArgs } from '../../../common/dtos/args/skip-limit.args.dto';
import { SortOrderEnum } from '../../../common/enum/sort-order.enum';

@ArgsType()
export class WatchingListArgs extends SkipLimitArgs {
  @Field(() => String, { nullable: true })
  @IsOptional()
  userId?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  companyId?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  jobName?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  sortBy?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  sortOrder?: SortOrderEnum;
}
