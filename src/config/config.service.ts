import { Injectable } from '@nestjs/common';
import { ConfigAppProviderType } from './@types/config-app.type';
import { ConfigEnvType } from './@types/config-env.type';

@Injectable()
export class ConfigService {
  get(): ConfigAppProviderType {
    const {
      DATABASE_CONNECTION,
      DATABASE_USERNAME,
      DATABASE_PASSWORD,
      DATABASE_AUTH_SOURCE,
      JWT_SECRET_KEY,
      JWT_EXPIRES,
      ORIGIN,
      BCRYPT_SECRET_ROUND,
      PORT,
      SESSION_EXPIRES,
      SESSION_SECRET,
      HOSTNAME,
      MAIN_HOSTNAME,
      MAILER_USER,
      MAILER_PASSWORD,
      FLARUM_API_KEY,
      FLARUM_ADMIN_ID,
      FLARUM_URL,
    } = process.env as ConfigEnvType;
    return {
      hostname: HOSTNAME,
      main_hostname: MAIN_HOSTNAME,
      mailer: {
        user: MAILER_USER || 'lapis.mailer@gmail.com',
        password: MAILER_PASSWORD || '100310571067',
      },
      database: {
        connection:
          DATABASE_CONNECTION ||
          `mongodb+srv://root:Hay4mh6eyHaeQ1Qi@senior-pro.mtbns.mongodb.net/senior-pro?retryWrites=true&w=majority`,
        username: DATABASE_USERNAME,
        password: DATABASE_PASSWORD,
        authSource: DATABASE_AUTH_SOURCE || 'admin',
      },
      jwt: {
        secretKey: JWT_SECRET_KEY || 'jwt1secret',
        expires: Number(JWT_EXPIRES || 300000), // 5 minutes
      },
      node_env:
        (process.env.NODE_ENV as ConfigAppProviderType['node_env']) ||
        'development',
      origin: new RegExp(ORIGIN || 'http://localhost:5002'),
      bcrypt: {
        round: Number(BCRYPT_SECRET_ROUND || 10),
      },
      port: Number(PORT || 5000),
      session: {
        secret: SESSION_SECRET || 'session1secret',
        expires: Number(SESSION_EXPIRES || 86400000), // one day
      },
      flarum: {
        apiKey: FLARUM_API_KEY || 'h4OjO9toTYTg7w32NIZhZEh2RN0QpHOWVykInb2m',
        adminId: Number(FLARUM_ADMIN_ID) || 1,
        url: FLARUM_URL || 'https://forum.lalapis.me',
      },
    };
  }
}
