export type ConfigAppProviderType = {
  hostname: string;
  main_hostname: string;
  mailer: {
    user: string;
    password: string;
  };
  database: {
    connection: string;
    username: string;
    password: string;
    authSource: string;
  };
  jwt: {
    secretKey: string;
    expires: number;
  };
  node_env: 'production' | 'development';
  origin: RegExp;
  bcrypt: {
    round: number;
  };
  port: number;
  session: {
    secret: string;
    expires: number;
  };
  flarum: {
    apiKey: string;
    adminId: number;
    url: string;
  };
};
