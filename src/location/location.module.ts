import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../config/config.module';
import locationSchema from './entity/location.entity';
import { LocationResolver } from './location.resolver';
import { LocationService } from './location.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'locations',
        schema: locationSchema,
      },
    ]),
    ConfigModule,
  ],
  providers: [LocationService, LocationResolver],
  exports: [LocationService, ConfigModule],
})
export class LocationModule {}
