import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LocationDocument } from './entity/location.entity';

@Injectable()
export class LocationService {
  constructor(
    @InjectModel('locations')
    private readonly locationModel: Model<LocationDocument>,
  ) {}

  async getLocations(): Promise<LocationDocument> {
    return this.locationModel.find().lean();
  }

  async getById(id: string): Promise<LocationDocument> {
    return this.locationModel.findById(id).lean();
  }
}
