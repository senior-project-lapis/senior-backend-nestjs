export type LocationSchema = {
  _id: string;
  name: string;
};
