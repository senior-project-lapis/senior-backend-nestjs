import { Args, Query, Resolver } from '@nestjs/graphql';
import { Location } from 'graphql';
import { LocationService } from './location.service';
import { LocaitionWithId } from './models/location.model';

@Resolver(() => Location)
export class LocationResolver {
  constructor(private readonly locationService: LocationService) {}

  // location
  @Query(() => LocaitionWithId, { name: 'Location', nullable: true })
  async GetLocation(@Args('id', { type: () => String }) id: string) {
    return this.locationService.getById(id);
  }

  // locations
  @Query(() => [LocaitionWithId], { name: 'Locations' })
  async GetLocations() {
    return this.locationService.getLocations();
  }
}
