/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType } from '@nestjs/graphql';
import { ILocation } from '../interfaces/location';

@ObjectType()
export class Locaition implements ILocation {
  @Field(() => String)
  name: string;
}

@ObjectType()
export class LocaitionWithId extends Locaition {
  @Field(() => String)
  _id: string;
}
