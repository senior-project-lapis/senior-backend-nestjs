import { Document, Schema } from 'mongoose';
import { LocationSchema } from '../schema/location';

const locationSchema = new Schema<LocationDocument>(
  {
    name: { type: String, required: true },
  },
  { versionKey: false },
);

export type LocationDocument = Document & LocationSchema;

export default locationSchema;
