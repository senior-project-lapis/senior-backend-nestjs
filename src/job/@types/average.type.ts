/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int, ObjectType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@ObjectType()
export class AverageType {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  score: number;
}
