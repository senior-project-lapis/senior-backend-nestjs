/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int, ObjectType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@ObjectType()
export class TimelineTrendType {
  @Field(() => String)
  time: string;

  @Field(() => String)
  formattedTime: string;

  @Field(() => String)
  formattedAxisTime: string;

  @Field(() => [Number])
  value: number[];

  @Field(() => [Boolean])
  hasData: boolean[];

  @Field(() => [String])
  formattedValue: string[];
}
