/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int, ObjectType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { AverageType } from './average.type';
import { TimelineTrendType } from './timeline-trend.type';

@ObjectType()
export class JobTrendResponse {
  @Field(() => [String])
  keyword: string[];

  @Field(() => [TimelineTrendType])
  timelineData: TimelineTrendType[];

  @Field(() => String)
  startDate: Date;

  @Field(() => String)
  endDate: Date;

  @Field(() => [AverageType])
  averages: AverageType[];
}
