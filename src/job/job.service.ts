import {
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SourceTypeEnum } from '../common/enum/source-type.enum';
import { JobProperties } from './dtos/args/job-properties.args.dto';
import { CreateJobInputDto } from './dtos/inputs/create-job.input.dto';
import { UpdateJobInputDto } from './dtos/inputs/update-job.input.dto';
import { JobDocument, JobDocumentResponse } from './entity/job.entity';
import { JobNotExistsException } from '../common/exception/job.not-exists.exception';
import { Job } from './models/job.model';
import { JobStatusEnum } from './schema/enum/job-status.enum';
import googleTrends from 'google-trends-api';
import { JobTrendResponse } from './@types/job-trend-response.type';
import { JobTrendArgs } from './dtos/args/job-trend.args.dto';
import { TimelineTrendType } from './@types/timeline-trend.type';
import axios from 'axios';
import { CountryType } from './@types/country.type';
import { WatchingListService } from '../watching-list/watching-list.service';

@Injectable()
export class JobService {
  constructor(
    @InjectModel('jobs')
    private readonly jobModel: Model<JobDocument>,

    @Inject(forwardRef(() => WatchingListService))
    private readonly watchingListService: WatchingListService,
  ) {}

  async getJobs(jobProperties: JobProperties): Promise<JobDocumentResponse> {
    // TODO: Search with company properties
    const {
      name,
      jobType,
      location,
      category,
      salary,
      remote,
      skip,
      limit,
      companyId,
      educationLevel,
      status,
      sortBy,
      sortOrder,
      since,
    } = jobProperties;
    const query = this.jobModel.find();
    if (name) {
      const cleanName = name.replace(/[[\]{}()*+?\\^$|#\s]/g, '\\$&');
      const regex = new RegExp(cleanName, 'i');
      query.or([{ name: regex }, { companyName: regex }]);
    }
    if (jobType) {
      query.where('type', jobType);
    }
    if (location) {
      query.where('locations', location);
    }
    if (category) {
      query.where('categories', category);
    }
    if (companyId) {
      query.where('companyId', companyId);
    }
    if (educationLevel) {
      query.where('educationLevel', educationLevel);
    }
    if (status) {
      query.where('status', status);
    }
    if (salary) {
      query.where('salary').gte(salary);
    }
    if (remote !== undefined) {
      query.where('remote', remote);
    }
    if (since) {
      query.where('createAt', { $gte: new Date(since) });
    }
    if (sortBy && sortOrder) {
      query.sort([[sortBy, sortOrder]]);
    }
    const response: JobDocumentResponse = {
      totalCount: await query.clone().count(),
      jobResult: await query.skip(skip).limit(limit).lean(),
    };

    return response;
  }

  async getById(id: string): Promise<JobDocument> {
    return this.jobModel.findById(id).lean();
  }

  async getJobsCount(): Promise<number> {
    return this.jobModel.countDocuments({
      $or: [{ status: 'normal' }, { status: 'reported' }],
    });
  }

  async getReportedJobsCount(): Promise<number> {
    return this.jobModel.countDocuments({ status: 'reported' });
  }

  sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

  // ! Maybe here ?
  async getCountries(): Promise<CountryType[]> {
    const url =
      'https://pkgstore.datahub.io/core/country-list/data_csv/data/d7c9d7cfb42cb69f4422dec222dbbaa8/data_csv.csv';
    try {
      const response = await axios.get(url);
      const newData = response.data.split('\r\n').slice(1, -1);

      return newData.map((item) => {
        const splittedItem = item.replace('"', '').split(',');
        const code = splittedItem[splittedItem.length - 1];
        const name = splittedItem[0];
        return { name, code };
      });
    } catch (exception) {
      Logger.error(`ERROR received from ${url}: ${exception}\n`);
    }
    return [];
  }

  // ! Maybe here ?
  async getTrend(jobTrendArgs: JobTrendArgs): Promise<JobTrendResponse> {
    // Init constant
    const { geo, startYear, endYear } = jobTrendArgs;
    const startTime = new Date(Number(startYear), 1, 1);
    const endTime = new Date(Number(endYear), 1, 1);
    const allTimelineData: TimelineTrendType[] = [];
    const allAverages: number[] = [];

    // google-trend-api
    const keyword = [
      'Project manager',
      'Network architect',
      'Data analyst',
      'Data scientist',
      'Software engineer',
      'IT security engineer',
      'Help desk support',
      'UX designer',
      'Cloud security specialist',
      'Web developer',
    ];
    for (let round = 0; round < keyword.length / 5; round++) {
      const start = round * 5;
      const end = start + 5;
      const trendJsonData = await googleTrends.interestOverTime({
        keyword: keyword.slice(start, end),
        startTime,
        endTime,
        geo,
      });
      const { timelineData, averages } = JSON.parse(trendJsonData).default;
      allTimelineData.push(...timelineData);
      allAverages.push(...averages);
    }

    // Create averages score
    const newAverages = keyword
      .map((value, index) => ({
        name: value,
        score: allAverages[index],
      }))
      .sort((a, b) => (a.score > b.score ? -1 : 1));

    return {
      keyword,
      timelineData: allTimelineData,
      startDate: startTime,
      endDate: endTime,
      averages: newAverages,
    };
  }

  async findByIdList(idList: string[]): Promise<JobDocument> {
    return this.jobModel.find().where('_id').in(idList).lean();
  }

  async createJob(payload: CreateJobInputDto): Promise<JobDocument> {
    const job: Job = {
      ...payload,
      status: JobStatusEnum.normal,
      sourceType: SourceTypeEnum.internal,
      createAt: new Date(),
      updateAt: new Date(),
    };

    let doc;
    try {
      doc = new this.jobModel(job);
    } catch (err) {}

    const saved = await doc?.save();
    if (!saved) {
      Logger.error('job create error', 'MongoDB');
      throw new InternalServerErrorException('job create error');
    }
    this.watchingListService.sendAlert(payload);
    return saved;
  }

  async updateJob(payload: UpdateJobInputDto): Promise<JobDocument> {
    const updatePayload = { ...payload, id: undefined, updateAt: new Date() };
    const user = await this.jobModel.findByIdAndUpdate(
      payload.id,
      updatePayload,
      {
        new: true,
      },
    );
    if (!user) throw new JobNotExistsException();
    return user;
  }

  async removeJob(id: string): Promise<number> {
    const { deletedCount } = await this.jobModel.deleteOne({ _id: id });
    return deletedCount;
  }
}
