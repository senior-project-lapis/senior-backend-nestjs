/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field } from '@nestjs/graphql';

@ArgsType()
export class JobTrendArgs {
  @Field(() => String)
  startYear: string;

  @Field(() => String)
  endYear: string;

  @Field(() => String, { nullable: true })
  geo?: string;
}
