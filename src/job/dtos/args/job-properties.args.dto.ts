/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int } from '@nestjs/graphql';
import { SkipLimitArgs } from '../../../common/dtos/args/skip-limit.args.dto';
import { JobTypeEnum } from '../../schema/enum/job-type.enum';
import { SortOrderEnum } from '../../../common/enum/sort-order.enum';
import { JobStatusEnum } from '../../schema/enum/job-status.enum';

@ArgsType()
export class JobProperties extends SkipLimitArgs {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  jobType?: JobTypeEnum;

  @Field(() => String, { nullable: true })
  location?: string;

  @Field(() => String, { nullable: true })
  category?: string;

  @Field(() => Int, { nullable: true })
  salary?: number;

  @Field(() => String, { nullable: true })
  status?: JobStatusEnum;

  @Field(() => Boolean, { nullable: true })
  remote?: boolean;

  @Field(() => String, { nullable: true })
  companyId?: string;

  @Field(() => String, { nullable: true })
  educationLevel?: string;

  @Field(() => String, { nullable: true })
  sortBy?: string;

  @Field(() => String, { nullable: true })
  sortOrder?: SortOrderEnum;

  @Field(() => Date, { nullable: true })
  since?: Date;
}
