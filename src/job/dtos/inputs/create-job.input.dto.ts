import { Field, InputType } from '@nestjs/graphql';
import { MaxLength, MinLength, IsOptional } from 'class-validator';
import { Job } from '../../models/job.model';
import { ExperienceLevelEnum } from '../../schema/enum/experience-level.enum';
import { JobTypeEnum } from '../../schema/enum/job-type.enum';

@InputType()
export class CreateJobInputDto
  implements
    Pick<
      Job,
      | 'contents'
      | 'name'
      | 'shortName'
      | 'jobType'
      | 'locations'
      | 'categories'
      | 'experienceLevels'
      | 'companyId'
      | 'companyName'
      | 'applyUrl'
      | 'salary'
      | 'remote'
      | 'benefits'
      | 'requirements'
      | 'description'
      | 'educationLevel'
    >
{
  @Field(() => String, { nullable: true })
  @IsOptional()
  contents: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  @MinLength(1)
  @MaxLength(48)
  shortName: string;

  @Field(() => String)
  jobType: JobTypeEnum;

  @Field(() => [String])
  locations: string[];

  @Field(() => [String], { nullable: true })
  @IsOptional()
  categories: string[];

  @Field(() => [String])
  experienceLevels: ExperienceLevelEnum[];

  @Field(() => String)
  companyId: string;

  @Field(() => String)
  companyName: string;

  @Field(() => String)
  applyUrl: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  salary: string;

  @Field(() => String)
  remote: boolean;

  @Field(() => [String], { nullable: true })
  @IsOptional()
  benefits: string[];

  @Field(() => [String])
  requirements: string[];

  @Field(() => String)
  description: string;

  @Field(() => String)
  @MinLength(1)
  @MaxLength(60)
  educationLevel: string;
}
