import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, MaxLength, MinLength } from 'class-validator';
import { SourceTypeEnum } from '../../../common/enum/source-type.enum';
import { ExperienceLevelEnum } from '../../schema/enum/experience-level.enum';
import { JobStatusEnum } from '../../schema/enum/job-status.enum';
import { JobTypeEnum } from '../../schema/enum/job-type.enum';
import { CreateJobInputDto } from './create-job.input.dto';

@InputType()
export class UpdateJobInputDto implements CreateJobInputDto {
  @Field({})
  id: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  sourceType: SourceTypeEnum;

  @Field(() => String, { nullable: true })
  @IsOptional()
  contents: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  name: string;

  @Field(() => String, { nullable: true })
  @MinLength(1)
  @MaxLength(48)
  @IsOptional()
  shortName: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  jobType: JobTypeEnum;

  @Field(() => [String], { nullable: true })
  @IsOptional()
  locations: string[];

  @Field(() => [String], { nullable: true })
  @IsOptional()
  categories: string[];

  @Field(() => [String], { nullable: true })
  @IsOptional()
  experienceLevels: ExperienceLevelEnum[];

  @Field(() => String, { nullable: true })
  @IsOptional()
  companyId: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  companyName: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  applyUrl: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  salary: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  remote: boolean;

  @Field(() => [String], { nullable: true })
  @IsOptional()
  benefits: string[];

  @Field(() => [String], { nullable: true })
  @IsOptional()
  requirements: string[];

  @Field(() => String, { nullable: true })
  @IsOptional()
  description: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  educationLevel: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  status: JobStatusEnum;
}
