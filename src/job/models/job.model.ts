/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { JobTypeEnum } from '../schema/enum/job-type.enum';
import { ExperienceLevelEnum } from '../schema/enum/experience-level.enum';
import { IJob } from '../interfaces/job';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { JobStatusEnum } from '../schema/enum/job-status.enum';
// enums
registerEnumType(SourceTypeEnum, { name: 'SourceTypeEnum' });
registerEnumType(JobTypeEnum, { name: 'JobTypeEnum' });
registerEnumType(ExperienceLevelEnum, { name: 'ExperienceLevelEnum' });

@ObjectType()
export class Job implements IJob {
  @Field(() => SourceTypeEnum)
  sourceType: SourceTypeEnum;

  @Field(() => String, { nullable: true })
  sourceId?: string;

  @Field(() => String, { nullable: true })
  contents?: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  shortName: string;

  @Field(() => JobTypeEnum)
  jobType: JobTypeEnum;

  @Field(() => [String])
  locations: string[];

  @Field(() => [String], { nullable: true })
  categories?: string[];

  @Field(() => [ExperienceLevelEnum])
  experienceLevels: ExperienceLevelEnum[];

  @Field(() => String)
  companyId: string;

  @Field(() => String)
  companyName: string;

  @Field(() => String)
  applyUrl: string;

  @Field(() => String, { nullable: true })
  salary?: string;

  @Field(() => Boolean)
  remote: boolean;

  @Field(() => [String], { nullable: true })
  benefits?: string[];

  @Field(() => [String], { nullable: true })
  requirements?: string[];

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => String, { nullable: true })
  educationLevel?: string;

  @Field(() => String)
  status: JobStatusEnum;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class JobWithId extends Job {
  @Field(() => String)
  _id: string;
}

@ObjectType()
export class JobDocumentWithCount {
  @Field(() => [JobWithId])
  jobResult: JobWithId[];

  @Field(() => Number)
  totalCount: number;
}
