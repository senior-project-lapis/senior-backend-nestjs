import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../config/config.module';
import { WatchingListModule } from '../watching-list/watching-list.module';
import jobSchema from './entity/job.entity';
import { JobResolver } from './job.resolver';
import { JobService } from './job.service';

@Module({
  imports: [
    WatchingListModule,
    MongooseModule.forFeature([
      {
        name: 'jobs',
        schema: jobSchema,
      },
    ]),
    ConfigModule,
  ],
  providers: [JobService, JobResolver],
  exports: [JobService, ConfigModule],
})
export class JobModule {}
