import { JobTypeEnum } from './enum/job-type.enum';
import { ExperienceLevelEnum } from './enum/experience-level.enum';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { JobStatusEnum } from './enum/job-status.enum';

export type JobSchema = {
  _id: string;

  sourceType: SourceTypeEnum;
  sourceId: string;
  contents?: string;
  name: string;
  shortName: string;
  jobType: JobTypeEnum;
  locations: string[];
  categories?: string[];
  experienceLevels: ExperienceLevelEnum[];
  companyId: string;
  companyName: string;
  applyUrl: string;
  salary?: string;
  remote: boolean;
  benefits?: string[];
  requirements?: string[];
  description?: string;
  educationLevel?: string;
  status: JobStatusEnum;

  createAt: Date;
  updateAt: Date;
};
