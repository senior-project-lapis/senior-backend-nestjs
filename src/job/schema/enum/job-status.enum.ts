export enum JobStatusEnum {
  normal = 'normal',
  reported = 'reported',
  banned = 'banned',
}
