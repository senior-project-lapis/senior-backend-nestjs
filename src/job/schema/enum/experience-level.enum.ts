export enum ExperienceLevelEnum {
  internship = 'internship',
  entry = 'entry',
  mid = 'mid',
  senior = 'senior',
  management = 'management',
}
