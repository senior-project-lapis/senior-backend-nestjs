import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CountryType } from './@types/country.type';
import { JobTrendResponse } from './@types/job-trend-response.type';
import { JobProperties } from './dtos/args/job-properties.args.dto';
import { JobTrendArgs } from './dtos/args/job-trend.args.dto';
import { CreateJobInputDto } from './dtos/inputs/create-job.input.dto';
import { UpdateJobInputDto } from './dtos/inputs/update-job.input.dto';
import { JobService } from './job.service';
import { Job, JobDocumentWithCount, JobWithId } from './models/job.model';

@Resolver(() => Job)
export class JobResolver {
  constructor(private readonly jobService: JobService) {}

  // job
  @Query(() => JobWithId, { name: 'Job', nullable: true })
  async GetJob(@Args('id', { type: () => String }) id: string) {
    return this.jobService.getById(id);
  }

  // jobs
  @Query(() => JobDocumentWithCount, { name: 'Jobs' })
  async GetJobs(@Args() jobProperties: JobProperties) {
    return this.jobService.getJobs(jobProperties);
  }

  // ! job trend
  @Query(() => JobTrendResponse, { name: 'JobTrend' })
  async GetJobTrend(@Args() jobTrendArgs: JobTrendArgs) {
    return this.jobService.getTrend(jobTrendArgs);
  }

  // ! country
  @Query(() => [CountryType], { name: 'Countries' })
  async GetCountries() {
    return this.jobService.getCountries();
  }

  @Mutation(() => JobWithId)
  async CreateJob(
    @Args('CreateJobInputDto') createJobInputDto: CreateJobInputDto,
  ) {
    return this.jobService.createJob(createJobInputDto);
  }

  @Mutation(() => JobWithId)
  async UpdateJob(
    @Args('UpdateJobInputDto') updateJobInputDto: UpdateJobInputDto,
  ) {
    return this.jobService.updateJob(updateJobInputDto);
  }

  @Mutation(() => Number)
  async RemoveJob(@Args('id', { type: () => String }) id: string) {
    return this.jobService.removeJob(id);
  }
}
