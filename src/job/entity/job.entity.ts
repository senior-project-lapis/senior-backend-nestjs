import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema } from 'mongoose';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { JobWithId } from '../models/job.model';
import { ExperienceLevelEnum } from '../schema/enum/experience-level.enum';
import { JobStatusEnum } from '../schema/enum/job-status.enum';
import { JobTypeEnum } from '../schema/enum/job-type.enum';

import { JobSchema } from '../schema/job';

export const experienceLevelConstant: ExperienceLevelEnum[] = [
  ExperienceLevelEnum.internship,
  ExperienceLevelEnum.entry,
  ExperienceLevelEnum.mid,
  ExperienceLevelEnum.senior,
  ExperienceLevelEnum.management,
];

export const sourceTypeConstant: SourceTypeEnum[] = [
  SourceTypeEnum.external,
  SourceTypeEnum.internal,
];

export const jobTypeConstant: JobTypeEnum[] = [
  JobTypeEnum.external,
  JobTypeEnum.internal,
];

export const jobStatusConstant: JobStatusEnum[] = [
  JobStatusEnum.normal,
  JobStatusEnum.reported,
  JobStatusEnum.banned,
];

const jobSchema = new Schema<JobDocument>(
  {
    sourceType: { type: String, required: true, enum: sourceTypeConstant },
    sourceId: { type: String, required: false },
    contents: { type: String, required: false },
    name: { type: String, required: true },
    shortName: { type: String, required: true },

    jobType: { type: String, required: true, enum: jobTypeConstant },
    locations: { type: [String], required: true },
    categories: { type: [String], required: false },
    experienceLevels: {
      type: [String],
      required: true,
      enum: experienceLevelConstant,
    },
    companyId: { type: String, required: true },
    companyName: { type: String, required: true },
    applyUrl: { type: String, required: true },
    salary: { type: String, required: false },
    remote: { type: Boolean, required: true },
    benefits: { type: [String], required: false },
    requirements: { type: [String], required: false },
    description: { type: String, required: false },
    educationLevel: { type: String, required: false },
    status: { type: String, required: true, enum: jobStatusConstant },

    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type JobDocument = Document & JobSchema;

// TODO: do for all gets search
export type JobDocumentResponse = {
  jobResult: JobDocument;
  totalCount: number;
};

export default jobSchema;
