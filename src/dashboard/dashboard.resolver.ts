import { Query, Resolver } from '@nestjs/graphql';
import { ConfigService } from '../config/config.service';
import { UseGuards } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardStatDto } from './dtos/dashboard-stat.dto';
import { LastestJobDto } from './dtos/lastest-job.dto';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';

@Resolver(() => DashboardStatDto)
export class DashboardResolver {
  constructor(
    private configService: ConfigService,
    private readonly dashboardService: DashboardService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Query(() => DashboardStatDto, { name: 'DashboardStat' })
  async DashboardStat(): Promise<DashboardStatDto> {
    return this.dashboardService.getDashboardStat();
  }

  @UseGuards(JwtAuthGuard)
  @Query(() => [LastestJobDto], { name: 'LastestJobStat' })
  async LastestJobStat(): Promise<Array<LastestJobDto>> {
    return this.dashboardService.getLastestJobStat();
  }
}
