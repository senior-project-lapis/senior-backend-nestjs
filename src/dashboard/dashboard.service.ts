import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { PublisherService } from '../publisher/publisher.service';
import { JobService } from '../job/job.service';
import { CompanyService } from '../company/company.service';
import { DashboardStatDto } from './dtos/dashboard-stat.dto';
import { JobProperties } from '../job/dtos/args/job-properties.args.dto';
import { LastestJobDto } from './dtos/lastest-job.dto';

@Injectable()
export class DashboardService {
  constructor(
    private readonly jobService: JobService,
    private readonly companyService: CompanyService,
    private readonly userService: UserService,
    private readonly publisherService: PublisherService,
  ) {}

  subtractYears(numOfYears, date = new Date()): Date {
    date.setFullYear(date.getFullYear() - numOfYears);
    return date;
  }

  async getLastestJobStat(): Promise<Array<LastestJobDto>> {
    const jobProperties: JobProperties = {
      name: undefined,
      jobType: undefined,
      location: undefined,
      category: undefined,
      salary: undefined,
      remote: undefined,
      companyId: undefined,
      status: undefined,
      skip: 0,
      limit: 0,
      since: this.subtractYears(1),
    };

    const monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];

    const jobs = await this.jobService.getJobs(jobProperties);
    const jobsObject = JSON.parse(JSON.stringify(jobs.jobResult));
    const countMonth = {};

    jobsObject.forEach((job) => {
      const newDate = new Date(job.createAt);
      const key = `${monthNames[newDate.getMonth()]}_${newDate.getFullYear()}`;
      countMonth[key] = key in countMonth ? countMonth[key] + 1 : 1;
    });

    const lastedJob = Object.keys(countMonth).map((key) => {
      return { name: key, count: countMonth[key] };
    });

    lastedJob.sort((a, b) => {
      const [aMonth, aYear] = a.name.split('_');
      const [bMonth, bYear] = b.name.split('_');
      if (aYear !== bYear) {
        return Number(aYear) - Number(bYear);
      }
      return monthNames.indexOf(aMonth) - monthNames.indexOf(bMonth);
    });

    return lastedJob;
  }

  async getDashboardStat(): Promise<DashboardStatDto> {
    const jobCount = await this.jobService.getJobsCount();
    const companyCount = await this.companyService.getCompaniesCount();
    const userCount = await this.userService.getUsersCount();
    const publisherCount = await this.publisherService.getPublishersCount();

    const reportedJobCount = await this.jobService.getReportedJobsCount();
    const waitingApprovePublisherCount =
      await this.publisherService.getWaitingApprovePublishersCount();

    const result: DashboardStatDto = {
      activeData: [
        {
          name: 'Reported Jobs',
          totalCount: reportedJobCount,
        },
        {
          name: 'Publisher waiting for approve',
          totalCount: waitingApprovePublisherCount,
        },
      ],
      nonActiveData: [
        {
          name: 'Jobs',
          totalCount: jobCount,
        },
        {
          name: 'Companies',
          totalCount: companyCount,
        },
        {
          name: 'Users',
          totalCount: userCount,
        },
        {
          name: 'Publishers',
          totalCount: publisherCount,
        },
      ],
    };
    return result;
  }
}
