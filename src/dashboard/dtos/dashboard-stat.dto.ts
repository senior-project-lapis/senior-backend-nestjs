/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { ActiveDataType } from '../@types/active-data.type';
import { NonActiveDataType } from '../@types/non-active-data.type copy';

@ObjectType()
export class DashboardStatDto {
  @Field(() => [NonActiveDataType])
  nonActiveData: NonActiveDataType[];

  @Field(() => [ActiveDataType])
  activeData: ActiveDataType[];
}
