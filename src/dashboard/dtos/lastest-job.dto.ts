/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';

@ObjectType()
export class LastestJobDto {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  count: number;
}
