import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ActiveDataType {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  totalCount: number;
}
