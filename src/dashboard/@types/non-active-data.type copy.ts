import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class NonActiveDataType {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  totalCount: number;
}
