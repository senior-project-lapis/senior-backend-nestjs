import { Module, forwardRef } from '@nestjs/common';
import { AuthenticationModule } from '../authentication/authentication.module';
import { CompanyModule } from '../company/company.module';
import { ConfigModule } from '../config/config.module';
import { JobModule } from '../job/job.module';
import { PublisherModule } from '../publisher/publisher.module';
import { UserModule } from '../user/user.module';
import { DashboardResolver } from './dashboard.resolver';
import { DashboardService } from './dashboard.service';

@Module({
  imports: [
    ConfigModule,
    forwardRef(() => UserModule),
    AuthenticationModule,
    JobModule,
    CompanyModule,
    UserModule,
    PublisherModule,
  ],
  providers: [DashboardService, DashboardResolver],
  exports: [DashboardService, ConfigModule],
})
export class DashboardModule {}
