import { Document, Schema } from 'mongoose';
import { CompanySizeEnum } from '../schema/enum/company-size.enum';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { CompanySchema } from '../schema/company';
import { CompanyStatusEnum } from '../schema/enum/company-status.enum';

export const sourceTypeConstant: SourceTypeEnum[] = [
  SourceTypeEnum.external,
  SourceTypeEnum.internal,
];

export const companyStatusConstant: CompanyStatusEnum[] = [
  CompanyStatusEnum.pending,
  CompanyStatusEnum.approved,
];

export const companySizeConstant: CompanySizeEnum[] = [
  CompanySizeEnum.small,
  CompanySizeEnum.medium,
  CompanySizeEnum.large,
];

const companySchema = new Schema<CompanyDocument>(
  {
    sourceType: { type: String, required: true, enum: sourceTypeConstant },

    sourceId: { type: String, required: false },
    description: { type: String, required: false },
    locations: { type: [String], required: true },
    industries: { type: [String], required: true },
    shortName: { type: String, required: true },
    name: { type: String, required: true },
    status: { type: String, required: true, enum: companyStatusConstant },
    size: { type: String, required: true, enum: companySizeConstant },
    logoImage: { type: String, required: false },
    properties: {
      type: [{ name: String, positive: Number, negative: Number }],
      required: false,
    },

    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type CompanyDocument = Document & CompanySchema;

export type CompanyDocumentResponse = {
  companyResult: CompanyDocument;
  totalCount: number;
};

export default companySchema;
