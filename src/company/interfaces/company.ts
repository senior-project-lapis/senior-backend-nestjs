import { CompanySizeEnum } from '../schema/enum/company-size.enum';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { CompanyProperty } from '../@types/company-property.type';
import { CompanyStatusEnum } from '../schema/enum/company-status.enum';

export interface ICompany {
  sourceType: SourceTypeEnum;
  sourceId?: string;
  description?: string;
  locations: string[];
  industries: string[];
  shortName: string;
  name: string;
  size: CompanySizeEnum;
  logoImage?: string;
  properties?: CompanyProperty[];
  status: CompanyStatusEnum;

  createAt: Date;
  updateAt: Date;
}
