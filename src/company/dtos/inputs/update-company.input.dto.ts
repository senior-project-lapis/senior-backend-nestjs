import { Field, InputType } from '@nestjs/graphql';
import { MaxLength, MinLength, IsOptional } from 'class-validator';
import { CompanySizeEnum } from '../../schema/enum/company-size.enum';
import { Company } from '../../models/company';
import { CompanyStatusEnum } from '../../schema/enum/company-status.enum';
import { CreateCompanyInputDto } from './create-company.input.dto';

@InputType()
export class UpdateCompanyInputDto implements CreateCompanyInputDto {
  @Field({})
  id: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  description: string;

  @Field(() => [String], { nullable: true })
  @IsOptional()
  industries: string[];

  @Field(() => [String], { nullable: true })
  @IsOptional()
  locations: string[];

  @Field(() => String, { nullable: true })
  @IsOptional()
  name: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  status: CompanyStatusEnum;

  @Field(() => String, { nullable: true })
  @IsOptional()
  size: CompanySizeEnum;

  @Field(() => String, { nullable: true })
  @IsOptional()
  logoImage: string;
}
