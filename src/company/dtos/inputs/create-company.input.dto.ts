import { Field, InputType } from '@nestjs/graphql';
import { MaxLength, MinLength, IsOptional } from 'class-validator';
import { CompanySizeEnum } from '../../schema/enum/company-size.enum';
import { Company } from '../../models/company';

@InputType()
export class CreateCompanyInputDto
  implements
    Pick<
      Company,
      'description' | 'locations' | 'industries' | 'name' | 'size' | 'logoImage'
    >
{
  @Field(() => String, { nullable: true })
  @IsOptional()
  description: string;

  @Field(() => [String])
  industries: string[];

  @Field(() => [String])
  locations: string[];

  @Field(() => String)
  @MinLength(1)
  @MaxLength(72)
  name: string;

  @Field(() => String)
  size: CompanySizeEnum;

  @Field(() => String, { nullable: true })
  @IsOptional()
  logoImage: string;
}
