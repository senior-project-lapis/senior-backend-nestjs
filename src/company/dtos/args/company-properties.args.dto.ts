/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int } from '@nestjs/graphql';
import { SkipLimitArgs } from '../../../common/dtos/args/skip-limit.args.dto';
import { CompanySizeEnum } from '../../schema/enum/company-size.enum';
import { SortOrderEnum } from '../../../common/enum/sort-order.enum';
import { CompanyStatusEnum } from '../../schema/enum/company-status.enum';

@ArgsType()
export class CompanyProperties extends SkipLimitArgs {
  @Field(() => String, { nullable: true })
  name: string;

  @Field(() => String, { nullable: true })
  size: CompanySizeEnum;

  @Field(() => String, { nullable: true })
  location: string;

  @Field(() => String, { nullable: true })
  industry: string;

  @Field(() => String, { nullable: true })
  status: CompanyStatusEnum;

  @Field(() => String, { nullable: true })
  sortBy: string;

  @Field(() => String, { nullable: true })
  sortOrder: SortOrderEnum;
}
