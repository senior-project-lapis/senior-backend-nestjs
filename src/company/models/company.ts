/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { CompanySizeEnum } from '../schema/enum/company-size.enum';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { CompanyProperty } from '../@types/company-property.type';
import { ICompany } from '../interfaces/company';
import { CompanyStatusEnum } from '../schema/enum/company-status.enum';

// enums
registerEnumType(SourceTypeEnum, { name: 'SourceTypeEnum' });
registerEnumType(CompanySizeEnum, { name: 'CompanySizeEnum' });

@ObjectType()
export class Company implements ICompany {
  @Field(() => SourceTypeEnum)
  sourceType: SourceTypeEnum;

  @Field(() => String, { nullable: true })
  sourceId?: string;

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => [String])
  locations: string[];

  @Field(() => [String])
  industries: string[];

  @Field(() => String)
  shortName: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  status: CompanyStatusEnum;

  @Field(() => CompanySizeEnum)
  size: CompanySizeEnum;

  @Field(() => String, { nullable: true })
  logoImage?: string;

  @Field(() => [CompanyProperty], { nullable: true })
  properties?: CompanyProperty[];

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class CompanyWithId extends Company {
  @Field(() => String)
  _id: string;
}

@ObjectType()
export class CompanyDocumentWithCount {
  @Field(() => [CompanyWithId])
  companyResult: CompanyWithId[];

  @Field(() => Number)
  totalCount: number;
}
