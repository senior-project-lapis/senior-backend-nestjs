import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../config/config.module';
import { CompanyResolver } from './company.resolver';
import { CompanyService } from './company.service';
import companySchema from './entity/company.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'companies',
        schema: companySchema,
      },
    ]),
    ConfigModule,
  ],
  providers: [CompanyService, CompanyResolver],
  exports: [CompanyService, ConfigModule],
})
export class CompanyModule {}
