import { Field, InputType } from '@nestjs/graphql';
import { CompanyWithId } from '../models/company';

@InputType()
export class CompanySampleDetailType
  implements Pick<CompanyWithId, '_id' | 'name'>
{
  @Field(() => String)
  _id: string;

  @Field(() => String)
  name: string;
}
