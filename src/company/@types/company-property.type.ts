/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int, ObjectType } from '@nestjs/graphql';
import { Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@ObjectType()
export class CompanyProperty {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  positive: number;

  @Field(() => Number)
  negative: number;
}
