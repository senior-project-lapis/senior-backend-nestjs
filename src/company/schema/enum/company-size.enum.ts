export enum CompanySizeEnum {
  small = 'small',
  medium = 'medium',
  large = 'large',
}
