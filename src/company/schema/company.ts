import { CompanySizeEnum } from './enum/company-size.enum';
import { SourceTypeEnum } from '../../common/enum/source-type.enum';
import { CompanyProperty } from '../@types/company-property.type';
import { CompanyStatusEnum } from './enum/company-status.enum';

export type CompanySchema = {
  _id: string;

  sourceType: SourceTypeEnum;
  sourceId?: string;
  description?: string;
  locations: string[];
  industries: string[];
  shortName: string;
  name: string;
  status: CompanyStatusEnum;
  size: CompanySizeEnum;
  logoImage?: string;
  properties?: CompanyProperty[];

  createAt: Date;
  updateAt: Date;
};
