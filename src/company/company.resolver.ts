import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { CompanyService } from './company.service';
import { CompanyProperties } from './dtos/args/company-properties.args.dto';
import { CreateCompanyInputDto } from './dtos/inputs/create-company.input.dto';
import { UpdateCompanyInputDto } from './dtos/inputs/update-company.input.dto';
import {
  Company,
  CompanyDocumentWithCount,
  CompanyWithId,
} from './models/company';

@Resolver(() => Company)
export class CompanyResolver {
  constructor(private readonly companyService: CompanyService) {}

  // company
  @Query(() => CompanyWithId, { name: 'Company', nullable: true })
  async GetCompany(@Args('id', { type: () => String }) id: string) {
    return this.companyService.getById(id);
  }

  // companies
  @Query(() => CompanyDocumentWithCount, { name: 'Companies' })
  async GetCompanies(@Args() companyProperties: CompanyProperties) {
    return this.companyService.getCompanies(companyProperties);
  }

  @Mutation(() => CompanyWithId)
  async CreateCompany(
    @Args('CreateCompanyInputDto') createCompanyInputDto: CreateCompanyInputDto,
  ) {
    return this.companyService.createCompany(createCompanyInputDto);
  }

  @Mutation(() => CompanyWithId)
  async UpdateCompany(
    @Args('UpdateCompanyInputDto') updateCompanyInputDto: UpdateCompanyInputDto,
  ) {
    return this.companyService.updateCompany(updateCompanyInputDto);
  }

  @Mutation(() => Number)
  async RemoveCompany(@Args('id', { type: () => String }) id: string) {
    return this.companyService.removeCompany(id);
  }
}
