import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SourceTypeEnum } from '../common/enum/source-type.enum';
import { CompanyNotExistsException } from '../common/exception/company.not-exists.exception';
import { CompanyProperties } from './dtos/args/company-properties.args.dto';
import { CreateCompanyInputDto } from './dtos/inputs/create-company.input.dto';
import { UpdateCompanyInputDto } from './dtos/inputs/update-company.input.dto';
import {
  CompanyDocument,
  CompanyDocumentResponse,
} from './entity/company.entity';
import { Company } from './models/company';
import { CompanyStatusEnum } from './schema/enum/company-status.enum';

@Injectable()
export class CompanyService {
  constructor(
    @InjectModel('companies')
    private readonly companyModel: Model<CompanyDocument>,
  ) {}

  async getCompanies(
    companyProperties: CompanyProperties,
  ): Promise<CompanyDocumentResponse> {
    const {
      name,
      size,
      location,
      industry,
      skip,
      limit,
      status,
      sortBy,
      sortOrder,
    } = companyProperties;
    const query = this.companyModel.find();

    if (name) {
      const nameRegex = new RegExp(name, 'i');
      query.where('name', nameRegex);
    }
    if (size) {
      query.where('size', size);
    }
    if (status) {
      query.where('status', status);
    }
    if (location) {
      query.where('locations', location);
    }
    if (industry) {
      const industryRegex = new RegExp(industry, 'i');
      query.where('industries', industryRegex);
    }

    const response: CompanyDocumentResponse = {
      totalCount: await query.clone().count(),
      companyResult: await query
        .sort([[sortBy, sortOrder]])
        .skip(skip)
        .limit(limit)
        .lean(),
    };
    return response;
  }

  async getById(id: string): Promise<CompanyDocument> {
    return this.companyModel.findById(id).lean();
  }

  async getCompaniesCount(): Promise<number> {
    return this.companyModel.countDocuments({ status: 'approved' });
  }

  async findByIdList(idList: string[] | undefined): Promise<Company[]> {
    if (!idList) return this.companyModel.find().where('_id').in([]).lean();
    return this.companyModel.find().where('_id').in(idList).lean();
  }

  async createCompany(
    payload: CreateCompanyInputDto,
  ): Promise<CompanyDocument> {
    const company: Company = {
      status: CompanyStatusEnum.pending,
      ...payload,
      shortName: payload.name.toLocaleLowerCase().replace(' ', '-'),
      sourceType: SourceTypeEnum.internal,
      createAt: new Date(),
      updateAt: new Date(),
    };

    let doc;
    try {
      doc = new this.companyModel(company);
    } catch (err) {}

    const saved = await doc?.save();
    if (!saved) {
      Logger.error('company create error', 'MongoDB');
      throw new InternalServerErrorException('company create error');
    }
    return saved;
  }

  async updateCompany(
    payload: UpdateCompanyInputDto,
  ): Promise<CompanyDocument> {
    const updatePayload = {
      ...payload,
      shortName: payload.name.toLocaleLowerCase().replace(' ', '-'),
      id: undefined,
      updateAt: new Date(),
    };
    const company = await this.companyModel.findByIdAndUpdate(
      payload.id,
      updatePayload,
      {
        new: true,
      },
    );
    if (!company) throw new CompanyNotExistsException();
    return company;
  }

  async removeCompany(id: string): Promise<number> {
    const { deletedCount } = await this.companyModel.deleteOne({ _id: id });
    return deletedCount;
  }
}
