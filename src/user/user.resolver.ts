import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Request } from 'express';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import { SkipLimitArgs } from '../common/dtos/args/skip-limit.args.dto';
import { UserExceptionFilters } from '../common/filters/user-error.filter';
import { UserInvalidCredentialException } from '../common/exception/user.invalid-credential.exception';
import { UserRegisterInputDto } from './dtos/inputs/user-register.input.dto';
import { UserUpdateInputDto } from './dtos/inputs/user-update.input.dto';
import { User, UserWithId } from './models/user.model';
import { UserService } from './user.service';

@Resolver(() => User)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  // user
  @Query(() => UserWithId, { name: 'User', nullable: true })
  async GetUser(@Args('id', { type: () => String }) id: string) {
    return this.userService.getById(id);
  }

  // user by session
  @UseGuards(JwtAuthGuard)
  @Query(() => UserWithId, { nullable: true })
  async GetUserBySession(@Context('req') req: Request) {
    let session;
    let id;
    try {
      session = req.session as Record<string, any>;
      id = session.user._id;
    } catch (error) {
      throw new UserInvalidCredentialException();
    }
    return this.userService.getById(id);
  }

  @UseFilters(UserExceptionFilters)
  @Mutation(() => UserWithId)
  async CreateUser(
    @Args('UserRegisterInputDto') userRegisterInputDto: UserRegisterInputDto,
  ) {
    return this.userService.createUser(userRegisterInputDto);
  }

  @Mutation(() => UserWithId)
  async UpdateUser(
    @Args('UserUpdateInputDto') userRegisterInputDto: UserUpdateInputDto,
  ) {
    return this.userService.updateUser(userRegisterInputDto);
  }

  // users
  @Query(() => [UserWithId], { name: 'Users' })
  async GetUsers(@Args() skipLimit: SkipLimitArgs) {
    const { skip, limit } = skipLimit;
    return this.userService.getUsers(skip, limit);
  }
}
