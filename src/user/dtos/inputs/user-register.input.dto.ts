import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, MaxLength, MinLength, IsOptional } from 'class-validator';
import { User } from '../../models/user.model';

@InputType()
export class UserRegisterInputDto
  implements
    Pick<
      User,
      | 'email'
      | 'password'
      | 'firstname'
      | 'lastname'
      | 'middlename'
      | 'dob'
      | 'phonenumber'
      | 'address'
    >
{
  @Field()
  @IsEmail()
  email: string;

  @Field()
  @MinLength(6)
  @MaxLength(48)
  password: string;

  @Field()
  @MinLength(1)
  @MaxLength(48)
  firstname: string;

  @Field({ nullable: true })
  @IsOptional()
  middlename: string;

  @Field()
  @MinLength(1)
  @MaxLength(48)
  lastname: string;

  @Field()
  dob: Date;

  @Field()
  @MinLength(10)
  @MaxLength(10)
  phonenumber: string;

  @Field()
  address: string;

  @Field({ nullable: true })
  @IsOptional()
  description: string;
}
