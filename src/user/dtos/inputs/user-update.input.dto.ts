import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsOptional } from 'class-validator';
import {
  EducationDetail,
  EducationDetailInput,
} from '../../@types/education.type';
import { ExperienceInput, Experience } from '../../@types/experience.type';
import { Skill } from '../../@types/skill.type';
import { UserRegisterInputDto } from './user-register.input.dto';

@InputType()
export class UserUpdateInputDto
  implements Omit<UserRegisterInputDto, 'password'>
{
  @Field({})
  id: string;

  @Field({ nullable: true })
  @IsEmail()
  @IsOptional()
  email: string;

  @Field({ nullable: true })
  @IsOptional()
  password: string;

  @Field({ nullable: true })
  @IsOptional()
  firstname: string;

  @Field({ nullable: true })
  @IsOptional()
  middlename: string;

  @Field({ nullable: true })
  @IsOptional()
  lastname: string;

  @Field({ nullable: true })
  @IsOptional()
  dob: Date;

  @Field({ nullable: true })
  @IsOptional()
  phonenumber: string;

  @Field({ nullable: true })
  @IsOptional()
  address: string;

  @Field(() => [EducationDetailInput], { nullable: true })
  @IsOptional()
  education: EducationDetail[];

  @Field(() => [ExperienceInput], { nullable: true })
  @IsOptional()
  experience: Experience[];

  @Field(() => [Skill], { nullable: true })
  @IsOptional()
  skill: Skill[];

  @Field(() => String, { nullable: true })
  @IsOptional()
  description: string;
}
