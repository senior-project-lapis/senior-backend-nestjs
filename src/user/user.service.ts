import {
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { genSalt, hash } from 'bcryptjs';
import { UserRoleEnum } from './schema/enum/user-role.enum';
import { UserDocument } from './entity/user.entity';
import { LeanDocument, Model } from 'mongoose';

import { UserNotExistsException } from '../common/exception/user.not-exists.exception';
import { ConfigService } from '../config/config.service';
import { UserRegisterInputDto } from './dtos/inputs/user-register.input.dto';
import { UserUpdateInputDto } from './dtos/inputs/user-update.input.dto';
import {
  UserQueryReturns,
  UsersQueryReturns,
} from './models/user-query-returns.model';
import { User } from './models/user.model';
import { DuplcatedEmailException } from '../common/exception/duplicated-email.exception';
import { PublisherService } from '../publisher/publisher.service';
import axios from 'axios';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('users')
    private readonly userModel: Model<UserDocument>,
    @Inject(forwardRef(() => PublisherService))
    private readonly publisherService: PublisherService,
    private readonly configService: ConfigService,
  ) {}

  async getUsers(skip = 0, limit = 1000): Promise<UsersQueryReturns> {
    return this.userModel
      .find({})
      .select({ password: 0 })
      .skip(skip)
      .limit(limit)
      .lean();
  }

  async getUsersCount(): Promise<number> {
    return this.userModel.countDocuments({ role: 'user' });
  }

  async getById(id: string): Promise<UserQueryReturns> {
    return this.userModel.findById(id).select({ password: 0 }).lean();
  }

  async checkExistUser(email: string): Promise<boolean> {
    let isExist: any = null;
    isExist = await this.userModel.exists({ email });
    return isExist;
  }

  async createUser(payload: UserRegisterInputDto): Promise<UserDocument> {
    const existUser = await this.checkExistUser(payload.email);
    const existPublisher = await this.publisherService.checkExistPublisher(
      payload.email,
    );

    if (existUser || existPublisher)
      throw new DuplcatedEmailException('Found existing email');

    const { round } = this.configService.get().bcrypt;
    const salt = await genSalt(round);
    const user: User = {
      ...payload,
      password: await hash(payload.password, salt),
      role: UserRoleEnum.user,

      createAt: new Date(),
      updateAt: new Date(),
    };

    let doc;
    try {
      doc = new this.userModel(user);
    } catch (err) {}

    const saved = await doc?.save();
    if (!saved) {
      Logger.error('user create error', 'MongoDB');
      throw new InternalServerErrorException('user create error');
    }

    // Create flarum user
    const flarumData = this.configService.get().flarum;
    const createUserBody = {
      data: {
        attributes: {
          username: payload.email,
          password: payload.password,
          email: payload.email,
        },
      },
    };
    const flarumHeaders = {
      headers: {
        Authorization:
          'Token ' + flarumData.apiKey + '; userId=' + flarumData.adminId,
      },
    };
    const flarumResponse = await axios.post(
      `${flarumData.url}/api/users`,
      createUserBody,
      flarumHeaders,
    );
    if (!flarumResponse.data) {
      Logger.error('flarum user create error', 'flarum');
      throw new InternalServerErrorException('flarum user create error');
    }
    return saved;
  }

  async updateUser(payload: UserUpdateInputDto): Promise<UserDocument> {
    const { round } = this.configService.get().bcrypt;
    const salt = await genSalt(round);
    const updatePayload = {
      ...payload,
      id: undefined,
      password: payload.password
        ? await hash(payload.password, salt)
        : undefined,
      updateAt: new Date(),
    };
    const user = await this.userModel.findByIdAndUpdate(
      payload.id,
      updatePayload,
      {
        new: true,
      },
    );
    if (!user) throw new UserNotExistsException();
    return user;
  }

  async getUserByEmail(email: string): Promise<LeanDocument<UserDocument>> {
    const user = await this.userModel.findOne({ email: email }).lean();
    if (!user) throw new UserNotExistsException();
    return user;
  }

  async removeUser(id: string): Promise<number> {
    const { deletedCount } = await this.userModel.deleteOne({ _id: id });
    return deletedCount;
  }
}
