/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Field,
  InputType,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { IsEnum } from 'class-validator';

@ObjectType('SkillType')
@InputType('SkillInput')
export class Skill {
  @Field(() => String)
  name: string;
}
