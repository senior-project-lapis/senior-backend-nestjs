/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Field,
  Int,
  InputType,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';

import { EducationLevelEnum } from '../schema/enum/education.enum';
import { IsEnum } from 'class-validator';

registerEnumType(EducationLevelEnum, {
  name: 'EducationLevelEnum',
});
@ObjectType('EducationDetailType')
export class EducationDetail {
  @Field(() => String)
  school: string;

  @Field(() => Int)
  graduationYear: number;

  @IsEnum(EducationLevelEnum)
  @Field((type) => EducationLevelEnum)
  educationLevel: EducationLevelEnum;
}

@InputType('EducationDetailInput')
export class EducationDetailInput {
  @Field(() => String)
  school: string;

  @Field(() => String)
  graduationYear: number;

  @IsEnum(EducationLevelEnum)
  @Field((type) => EducationLevelEnum)
  educationLevel: EducationLevelEnum;
}
