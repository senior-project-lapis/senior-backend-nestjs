/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, Int, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType('ExperienceType')
export class Experience {
  @Field(() => String)
  name: string;

  @Field(() => Int)
  year: number;

  @Field(() => String, { nullable: true })
  description?: string;
}

@InputType('ExperienceInput')
export class ExperienceInput {
  @Field(() => String)
  name: string;

  @Field(() => String)
  year: number;

  @Field(() => String, { nullable: true })
  description?: string;
}
