import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import userSchema from './entity/user.entity';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ConfigModule } from '../config/config.module';
import { UserResolver } from './user.resolver';
import { UserService } from './user.service';
import { PublisherModule } from '../publisher/publisher.module';

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature([
      {
        name: 'users',
        schema: userSchema,
      },
    ]),
    AuthenticationModule,
    forwardRef(() => PublisherModule),
  ],
  providers: [UserService, UserResolver],
  exports: [UserService, ConfigModule],
})
export class UserModule {}
