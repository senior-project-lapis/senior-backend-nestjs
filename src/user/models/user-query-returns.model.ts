import { UserDocument } from '../entity/user.entity';

export type UserQueryReturns = Omit<UserDocument, 'password'> | null;
export type UsersQueryReturns = Omit<UserDocument, 'password'>[] | [];
