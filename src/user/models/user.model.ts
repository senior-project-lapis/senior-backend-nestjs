/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { UserRoleEnum } from '../schema/enum/user-role.enum';
import { IUser } from '../interfaces/user';
import { Experience } from '../@types/experience.type';
import { Skill } from '../@types/skill.type';
import { EducationDetail } from '../@types/education.type';

// enums
registerEnumType(UserRoleEnum, { name: 'UserRoleEnum' });

@ObjectType()
export class User implements IUser {
  @Field(() => String)
  email: string;

  @Field(() => String)
  password: string;

  @Field(() => String)
  firstname: string;

  @Field({ nullable: true })
  middlename?: string;

  @Field(() => String)
  lastname: string;

  @Field(() => Date)
  dob: Date;

  @Field(() => String)
  phonenumber: string;

  @Field(() => String)
  address: string;

  // logic
  @Field(() => UserRoleEnum)
  role: UserRoleEnum;

  @Field(() => [EducationDetail], { nullable: true })
  education?: EducationDetail[];

  @Field(() => [Experience], { nullable: true })
  experience?: Experience[];

  @Field(() => [Skill], { nullable: true })
  skill?: Skill[];

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class UserWithId extends User {
  @Field(() => String)
  _id: string;
}
