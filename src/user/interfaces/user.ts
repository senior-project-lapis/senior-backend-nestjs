import { EducationDetail } from '../@types/education.type';
import { Experience } from '../@types/experience.type';
import { Skill } from '../@types/skill.type';
import { UserRoleEnum } from '../schema/enum/user-role.enum';

export interface IUser {
  email: string;
  password: string;
  firstname: string;
  middlename?: string;
  lastname: string;
  dob: Date;
  phonenumber: string;
  address: string;
  role: UserRoleEnum;
  education?: EducationDetail[];
  experience?: Experience[];
  skill?: Skill[];
  description?: string;
  createAt: Date;
  updateAt: Date;
}
