import { EducationDetail } from '../@types/education.type';
import { Experience } from '../@types/experience.type';
import { Skill } from '../@types/skill.type';
import { UserRoleEnum } from './enum/user-role.enum';
export type UserSchema = {
  _id: string;

  email: string;
  password: string;
  firstname: string;
  middlename?: string;
  lastname: string;
  dob: Date;
  phonenumber: string;
  address: string;

  //logic
  role: UserRoleEnum;

  education?: EducationDetail[];
  experience?: Experience[];
  skill?: Skill[];
  description?: string;

  createAt: Date;
  updateAt: Date;
};
