export enum SkillLevelEnum {
  introductory = 'Introductory',
  intermediate = 'Intermediate',
  advanced = 'Advanced',
}
