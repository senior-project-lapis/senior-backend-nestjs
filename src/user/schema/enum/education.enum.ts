export enum EducationLevelEnum {
  lessThanHighSchool = 'Less Than High School',
  highShool = 'High School or Equivalent',
  associate = `Associate's Degree`,
  bachelor = `Bachelor's Degree`,
  master = `Master's Degree`,
  doctoral = 'Doctoral Degree',
}
