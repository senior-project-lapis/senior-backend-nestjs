import { Document, Schema } from 'mongoose';
import { UserSchema } from '../schema/user';
import { UserRoleEnum } from '../schema/enum/user-role.enum';
import { EducationLevelEnum } from '../schema/enum/education.enum';
export const userRoleConstant: UserRoleEnum[] = [
  UserRoleEnum.admin,
  UserRoleEnum.user,
  UserRoleEnum.publisher,
];

export const educationLevelConstant: EducationLevelEnum[] = [
  EducationLevelEnum.associate,
  EducationLevelEnum.bachelor,
  EducationLevelEnum.doctoral,
  EducationLevelEnum.highShool,
  EducationLevelEnum.lessThanHighSchool,
  EducationLevelEnum.master,
];

const userSchema = new Schema<UserDocument>(
  {
    email: { type: String, unique: true },
    password: { type: String, required: true },
    firstname: { type: String, required: true },
    middlename: { type: String, required: false },
    lastname: { type: String, required: true },
    dob: { type: Date, required: true },
    phonenumber: { type: String, required: true },
    address: { type: String, required: true },
    // logic
    role: {
      type: String,
      require: true,
      enum: userRoleConstant,
    },
    education: {
      type: [
        {
          school: String,
          graduationYear: Number,
          educationLevel: { type: String, enum: educationLevelConstant },
        },
      ],
      required: false,
      _id: false,
    },
    experience: {
      type: [
        {
          name: String,
          year: Number,
          description: { type: String, required: false },
        },
      ],
      required: false,
      _id: false,
    },
    skill: {
      type: [{ name: String }],
      required: false,
      _id: false,
    },
    description: { type: String, required: false },
    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type UserDocument = Document & UserSchema;

export default userSchema;
