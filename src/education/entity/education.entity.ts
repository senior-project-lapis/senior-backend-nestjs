import { Document, Schema } from 'mongoose';
import { EducationSchema } from '../schema/education';

const educationSchema = new Schema<EducationDocument>(
  {
    name: { type: String, required: true },
    level: { type: Number, required: true },

    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type EducationDocument = Document & EducationSchema;

export default educationSchema;
