import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { JobDocument } from '../job/entity/job.entity';
import { EducationDocument } from './entity/education.entity';

@Injectable()
export class EducationService {
  constructor(
    @InjectModel('educations')
    private readonly educationModel: Model<EducationDocument>,
  ) {}

  async getEducationLevels(): Promise<JobDocument> {
    return this.educationModel.find().lean();
  }

  async getByLevel(level: number): Promise<EducationDocument> {
    return this.educationModel.findOne({ level }).lean();
  }

  async getById(id: string): Promise<EducationDocument> {
    return this.educationModel.findById(id).lean();
  }
}
