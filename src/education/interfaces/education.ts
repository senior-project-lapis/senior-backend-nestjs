export interface IEducation {
  name: string;
  level: number;

  createAt: Date;
  updateAt: Date;
}
