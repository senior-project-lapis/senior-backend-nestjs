import { Field, InputType } from '@nestjs/graphql';
import { Education } from '../../models/education.model';

@InputType()
export class CreateEducationInputDto
  implements Pick<Education, 'name' | 'level'>
{
  @Field(() => String)
  name: string;

  @Field(() => Number)
  level: number;
}
