import { Args, Query, Resolver } from '@nestjs/graphql';
import { EducationService } from './education.service';
import { Education, EducationWithId } from './models/education.model';

@Resolver(() => Education)
export class EducationResolver {
  constructor(private readonly educationService: EducationService) {}

  // education
  @Query(() => EducationWithId, { name: 'Education', nullable: true })
  async GetEducation(@Args('id', { type: () => String }) id: string) {
    return this.educationService.getById(id);
  }

  // education
  @Query(() => EducationWithId, { name: 'EducationByLevel', nullable: true })
  async GetEducationByLevel(
    @Args('level', { type: () => String }) level: number,
  ) {
    return this.educationService.getByLevel(level);
  }

  // educations
  @Query(() => [EducationWithId], { name: 'Educations' })
  async GetEducations() {
    return this.educationService.getEducationLevels();
  }
}
