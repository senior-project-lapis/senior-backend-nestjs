import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../config/config.module';
import { EducationResolver } from './education.resolver';
import { EducationService } from './education.service';
import educationSchema from './entity/education.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'educations',
        schema: educationSchema,
      },
    ]),
    ConfigModule,
  ],
  providers: [EducationService, EducationResolver],
  exports: [EducationService, ConfigModule],
})
export class EducationModule {}
