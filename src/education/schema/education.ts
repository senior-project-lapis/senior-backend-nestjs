export type EducationSchema = {
  _id: string;

  name: string;
  level: number;

  createAt: Date;
  updateAt: Date;
};
