/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { IEducation } from '../interfaces/education';

@ObjectType()
export class Education implements IEducation {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  level: number;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class EducationWithId extends Education {
  @Field(() => String)
  _id: string;
}
