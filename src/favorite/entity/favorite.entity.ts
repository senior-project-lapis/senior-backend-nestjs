import { Document, Schema } from 'mongoose';
import { FavoriteSchema } from '../schema/favorite';

const favoriteSchema = new Schema<FavoriteDocument>(
  {
    jobsId: { type: [String], required: true },
    userId: { type: String, required: true },

    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type FavoriteDocument = Document & FavoriteSchema;

export default favoriteSchema;
