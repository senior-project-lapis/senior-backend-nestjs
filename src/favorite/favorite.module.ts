import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ConfigModule } from '../config/config.module';
import { JobModule } from '../job/job.module';
import favoriteSchema from './entity/favorite.entity';
import { FavoriteResolver } from './favorite.resolver';
import { FavoriteService } from './favorite.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'favorites',
        schema: favoriteSchema,
      },
    ]),
    forwardRef(() => AuthenticationModule),
    JobModule,
    ConfigModule,
  ],
  providers: [FavoriteService, FavoriteResolver],
  exports: [FavoriteService, ConfigModule],
})
export class FavoriteModule {}
