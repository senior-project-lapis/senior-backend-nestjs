import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import { JobWithId } from '../job/models/job.model';
import { FavoriteJobInputDto } from './dtos/inputs/favorite-job.input.dto';
import { FavoriteService } from './favorite.service';
import { Favorite, FavoriteWithId } from './models/favorite.model';

@Resolver(() => Favorite)
export class FavoriteResolver {
  constructor(private readonly favoriteService: FavoriteService) {}

  // favorite
  @Query(() => FavoriteWithId, { name: 'Favorite', nullable: true })
  async GetFavorite(@Args('id', { type: () => String }) id: string) {
    return this.favoriteService.getById(id);
  }

  // favorites
  @Query(() => [JobWithId], { name: 'Favorites' })
  async GetFavoriteJobs(
    @Args('userId', { type: () => String }) userId: string,
  ) {
    return this.favoriteService.getFavoritesJobByUserId(userId);
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => FavoriteWithId)
  async CreateFavorite(
    @Args('FavoriteJobInputDto')
    createFavoriteJobInputDto: FavoriteJobInputDto,
  ) {
    return this.favoriteService.createFavorite(createFavoriteJobInputDto);
  }

  @UseGuards(JwtAuthGuard)
  @Mutation(() => FavoriteWithId)
  async RemoveFavorite(
    @Args('FavoriteJobInputDto')
    removeFavoriteJobInputDto: FavoriteJobInputDto,
  ) {
    return this.favoriteService.removeFavorite(removeFavoriteJobInputDto);
  }
}
