export type FavoriteSchema = {
  _id: string;

  jobsId: string[];
  userId: string;

  createAt: Date;
  updateAt: Date;
};
