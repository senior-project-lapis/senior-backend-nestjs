export interface IFavorite {
  jobsId: string[];
  userId: string;

  createAt: Date;
  updateAt: Date;
}
