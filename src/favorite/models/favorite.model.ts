/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType } from '@nestjs/graphql';
import { IFavorite } from '../interfaces/favorite';

@ObjectType()
export class Favorite implements IFavorite {
  @Field(() => [String])
  jobsId: string[];

  @Field(() => String)
  userId: string;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class FavoriteWithId extends Favorite {
  @Field(() => String)
  _id: string;
}
