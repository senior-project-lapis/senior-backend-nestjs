import { Field, InputType } from '@nestjs/graphql';
import { Favorite } from '../../models/favorite.model';

@InputType()
export class FavoriteJobInputDto implements Pick<Favorite, 'userId'> {
  @Field(() => String)
  jobId: string;

  @Field(() => String)
  userId: string;
}
