import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FavoriteDocument } from './entity/favorite.entity';
import { FavoriteJobInputDto } from './dtos/inputs/favorite-job.input.dto';
import { FavoriteNotExistsException } from '../common/exception/favorite.not-exists.exception';
import { JobService } from '../job/job.service';
import { JobDocument } from '../job/entity/job.entity';

@Injectable()
export class FavoriteService {
  constructor(
    @InjectModel('favorites')
    private readonly favoriteModel: Model<FavoriteDocument>,
    @Inject(forwardRef(() => JobService))
    private readonly jobService: JobService,
  ) {}

  async getFavoritesJobByUserId(userId: string): Promise<JobDocument> {
    const favoriteJobs = await this.favoriteModel
      .findOne({ userId }, { jobsId: 1, _id: 0 })
      .lean()
      .exec();
    if (!favoriteJobs) throw new FavoriteNotExistsException();
    return this.jobService.findByIdList(favoriteJobs.jobsId);
  }

  async getById(id: string): Promise<FavoriteDocument> {
    return this.favoriteModel.findById(id).lean();
  }

  async createFavorite(
    payload: FavoriteJobInputDto,
  ): Promise<FavoriteDocument> {
    const { userId, jobId } = payload;
    const favorite = await this.favoriteModel.findOneAndUpdate(
      { userId },
      { $addToSet: { jobsId: jobId }, updateAt: new Date() },
      { new: true, upsert: true },
    );
    if (!favorite) throw new FavoriteNotExistsException();
    return favorite;
  }

  async removeFavorite(
    payload: FavoriteJobInputDto,
  ): Promise<FavoriteDocument> {
    const { userId, jobId } = payload;
    const favorite = await this.favoriteModel.findOneAndUpdate(
      { userId },
      { $pull: { jobsId: jobId } },
      { new: true },
    );
    if (!favorite) throw new FavoriteNotExistsException();
    return favorite;
  }

  async deleteFavorite(id: string): Promise<number> {
    const { deletedCount } = await this.favoriteModel.deleteOne({ _id: id });
    return deletedCount;
  }
}
