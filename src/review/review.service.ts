import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ReviewDocument, ReviewDocumentResponse } from './entity/review.entity';
import { ReviewProperties } from './dtos/args/review-properties.input.dto';
import { CreateReviewInputDto } from './dtos/inputs/create-review.input.dto';
import { Review } from './models/review.model';

@Injectable()
export class ReviewService {
  constructor(
    @InjectModel('reviews')
    private readonly reviewModel: Model<ReviewDocument>,
  ) {}

  async getReviews(
    reviewProperties: ReviewProperties,
  ): Promise<ReviewDocumentResponse> {
    const { topic, sentiment, companyId, skip, limit, sortBy, sortOrder } =
      reviewProperties;
    const query = this.reviewModel.find();

    if (topic) {
      query.where('topic', topic);
    }
    if (sentiment) {
      query.where('sentiment', sentiment);
    }
    if (companyId) {
      query.where('companyId', companyId);
    }

    const response: ReviewDocumentResponse = {
      totalCount: await query.clone().count(),
      reviewResult: await query
        .sort([[sortBy, sortOrder]])
        .skip(skip)
        .limit(limit)
        .lean(),
    };
    return response;
  }

  async getById(id: string): Promise<ReviewDocument> {
    return this.reviewModel.findById(id).lean();
  }

  async createReview(payload: CreateReviewInputDto): Promise<ReviewDocument> {
    const review: Review = {
      ...payload,
      createAt: new Date(),
      updateAt: new Date(),
    };

    let doc;
    try {
      doc = new this.reviewModel(review);
    } catch (err) {}

    const saved = await doc?.save();
    if (!saved) {
      Logger.error('review create error', 'MongoDB');
      throw new InternalServerErrorException('review create error');
    }
    return saved;
  }

  //   async updateReview(payload: UpdateJobInputDto): Promise<JobDocument> {
  //     const updatePayload = { ...payload, id: undefined };
  //     const user = await this.jobModel.findByIdAndUpdate(
  //       payload.id,
  //       updatePayload,
  //       {
  //         new: true,
  //       },
  //     );
  //     if (!user) throw new JobNotExistsException();
  //     return user;
  //   }

  async removeReview(id: string): Promise<number> {
    const { deletedCount } = await this.reviewModel.deleteOne({ _id: id });
    return deletedCount;
  }
}
