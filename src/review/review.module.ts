import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '../config/config.module';
import reviewSchema from './entity/review.entity';
import { ReviewResolver } from './review.resolver';
import { ReviewService } from './review.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'reviews',
        schema: reviewSchema,
      },
    ]),
    ConfigModule,
  ],
  providers: [ReviewService, ReviewResolver],
  exports: [ReviewService, ConfigModule],
})
export class ReviewModule {}
