import { Document, Schema } from 'mongoose';
import { SentimentEnum } from '../schema/enum/sentiment.enum';
import { ReviewSchema } from '../schema/review';

export const sentimentConstant: SentimentEnum[] = [
  SentimentEnum.positive,
  SentimentEnum.negative,
];

const reviewSchema = new Schema<ReviewDocument>(
  {
    companyId: { type: String, required: true },
    title: { type: String, required: false },
    score: { type: Number, required: false },
    authorPosition: { type: String, required: true },
    content: { type: String, required: true },
    pro: { type: String, required: false },
    con: { type: String, required: false },

    topic: { type: String, required: false },
    sentiment: { type: String, required: false, enum: sentimentConstant },
    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type ReviewDocument = Document & ReviewSchema;

export type ReviewDocumentResponse = {
  reviewResult: ReviewDocument;
  totalCount: number;
};

export default reviewSchema;
