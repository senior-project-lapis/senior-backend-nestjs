import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ReviewProperties } from './dtos/args/review-properties.input.dto';
// import { CreateReviewInputDto } from './dtos/inputs/create-review.input.dto';
import {
  Review,
  ReviewDocumentWithCount,
  ReviewWithId,
} from './models/review.model';
import { ReviewService } from './review.service';

@Resolver(() => Review)
export class ReviewResolver {
  constructor(private readonly reviewService: ReviewService) {}

  // review
  @Query(() => ReviewWithId, { name: 'Review', nullable: true })
  async GetReview(@Args('id', { type: () => String }) id: string) {
    return this.reviewService.getById(id);
  }

  // reviews
  @Query(() => ReviewDocumentWithCount, { name: 'Reviews' })
  async GetReviews(@Args() reviewProperties: ReviewProperties) {
    return this.reviewService.getReviews(reviewProperties);
  }

  // @Mutation(() => ReviewWithId)
  // async CreateReview(
  //   @Args('CreateReviewInputDto') createReviewInputDto: CreateReviewInputDto,
  // ) {
  //   return this.reviewService.createReview(createReviewInputDto);
  // }

  @Mutation(() => Number)
  async RemoveReview(@Args('id', { type: () => String }) id: string) {
    return this.reviewService.removeReview(id);
  }
}
