/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { IReview } from '../interfaces/review';
import { SentimentEnum } from '../schema/enum/sentiment.enum';
// enums
registerEnumType(SentimentEnum, { name: 'SentimentEnum' });

@ObjectType()
export class Review implements IReview {
  @Field(() => String)
  companyId: string;

  @Field(() => String, { nullable: true })
  title?: string;

  @Field(() => Number, { nullable: true })
  score?: number;

  @Field(() => String, { nullable: true })
  authorPosition: string;

  @Field(() => String)
  content: string;

  @Field(() => String, { nullable: true })
  pro?: string;

  @Field(() => String, { nullable: true })
  con?: string;

  @Field(() => String, { nullable: true })
  topic?: string;

  @Field(() => SentimentEnum, { nullable: true })
  sentiment?: SentimentEnum;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class ReviewWithId extends Review {
  @Field(() => String)
  _id: string;
}

@ObjectType()
export class ReviewDocumentWithCount {
  @Field(() => [ReviewWithId])
  reviewResult: ReviewWithId[];

  @Field(() => Number)
  totalCount: number;
}
