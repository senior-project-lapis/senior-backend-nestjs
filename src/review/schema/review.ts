import { SentimentEnum } from './enum/sentiment.enum';

export type ReviewSchema = {
  _id: string;

  companyId: string;
  title?: string;
  score?: number;
  authorPosition: string;
  content: string;
  pro?: string;
  con?: string;

  topic?: string;
  sentiment?: SentimentEnum;

  createAt: Date;
  updateAt: Date;
};
