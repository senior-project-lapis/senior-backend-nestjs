import { Field, InputType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';
import { Review } from '../../models/review.model';

@InputType()
export class CreateReviewInputDto
  implements
    Pick<
      Review,
      'companyId' | 'title' | 'authorPosition' | 'content' | 'pro' | 'con'
    >
{
  @Field(() => String)
  companyId: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  title: string;

  @Field(() => String)
  authorPosition: string;

  @Field(() => String)
  content: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  pro: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  con: string;
}
