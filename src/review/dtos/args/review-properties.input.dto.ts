/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int } from '@nestjs/graphql';
import { SkipLimitArgs } from '../../../common/dtos/args/skip-limit.args.dto';
import { SortOrderEnum } from '../../../common/enum/sort-order.enum';
import { SentimentEnum } from '../../schema/enum/sentiment.enum';

@ArgsType()
export class ReviewProperties extends SkipLimitArgs {
  @Field(() => String, { nullable: true })
  topic: string;

  @Field(() => String, { nullable: true })
  sentiment: SentimentEnum;

  @Field(() => String, { nullable: true })
  companyId: string;

  @Field(() => String, { nullable: true })
  sortBy: string;

  @Field(() => String, { nullable: true })
  sortOrder: SortOrderEnum;
}
