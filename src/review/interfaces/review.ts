import { SentimentEnum } from '../schema/enum/sentiment.enum';

export interface IReview {
  companyId: string;
  title?: string;
  score?: number;
  authorPosition: string;
  content: string;
  pro?: string;
  con?: string;

  topic?: string;
  sentiment?: SentimentEnum;

  createAt: Date;
  updateAt: Date;
}
