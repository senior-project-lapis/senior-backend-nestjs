import { BadRequestException } from '@nestjs/common';

export class FavoriteNotExistsException extends BadRequestException {
  constructor(error?: string) {
    super('Favorite is not exists', error);
  }
}
