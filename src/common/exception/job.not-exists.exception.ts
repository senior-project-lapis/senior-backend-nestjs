import { BadRequestException } from '@nestjs/common';

export class JobNotExistsException extends BadRequestException {
  constructor(error?: string) {
    super('Job is not exists', error);
  }
}
