import { BadRequestException } from '@nestjs/common';

export class CompanyNotExistsException extends BadRequestException {
  constructor(error?: string) {
    super('company is not exists', error);
  }
}
