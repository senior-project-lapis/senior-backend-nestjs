import { BadRequestException } from '@nestjs/common';

export class UnapprovedPublisherException extends BadRequestException {
  constructor(error?: string) {
    super('Publisher account is unapproved', error);
  }
}
