import { BadRequestException } from '@nestjs/common';

export class WatchingListNotExistsException extends BadRequestException {
  constructor(error?: string) {
    super('Watching list is not exists', error);
  }
}
