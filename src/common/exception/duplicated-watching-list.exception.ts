import { BadRequestException } from '@nestjs/common';

export class DuplcatedWatchingListException extends BadRequestException {
  constructor(error?: string) {
    super('Duplicated watching list', error);
  }
}
