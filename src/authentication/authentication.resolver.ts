import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { Request, Response } from 'express';
import { AuthenticationService } from './authentication.service';
import { ConfigService } from '../config/config.service';
import { UserLoginInputDTO } from './dtos/user-login.input.dto';
import { UserSessionDTO } from './dtos/user-session.dto';
import { PublisherSessionDTO } from './dtos/publisher-session.dto';

@Resolver(() => UserSessionDTO)
export class AuthenticationResolver {
  constructor(
    private readonly authenticationService: AuthenticationService,
    private configService: ConfigService,
  ) {}

  @Mutation(() => UserSessionDTO)
  async Login(
    @Context('req') req: Request,
    @Args(UserLoginInputDTO.name) args: UserLoginInputDTO,
  ) {
    return this.authenticationService.userLogin(req, args);
  }

  @Mutation(() => PublisherSessionDTO)
  async LoginPublisher(
    @Context('req') req: Request,
    @Args(UserLoginInputDTO.name) args: UserLoginInputDTO,
  ) {
    return this.authenticationService.publisherLogin(req, args);
  }

  @Mutation(() => String, { nullable: true })
  async Logout(@Context('req') req: Request, @Context('res') res: Response) {
    req.session.destroy(() => ({}));
    res.clearCookie('Authorization', {
      httpOnly: true,
      domain: this.configService.get().main_hostname,
    });
    res.clearCookie('flarum_remember', {
      httpOnly: true,
      domain: this.configService.get().main_hostname,
    });
    return 'logged out';
  }

  @Mutation(() => String, { nullable: true })
  async RefreshToken(@Context('req') req: Request) {
    return this.authenticationService.refreshToken(req);
  }
}
