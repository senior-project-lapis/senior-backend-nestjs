import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
export type UserSessionDto = {
  _id: string;
  email: string;
  firstname: string;
  lastname: string;
  middlesname?: string;
  dob: Date;
  phonenumber: string;
  address: string;
  role: UserRoleEnum;
};
