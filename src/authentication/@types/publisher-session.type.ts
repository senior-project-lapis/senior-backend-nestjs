import { CompanySizeEnum } from '../../company/schema/enum/company-size.enum';
import { ApproveStatusEnum } from '../../publisher/schema/enum/approve-status.enum';
import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
export type PublisherSessionDto = {
  _id: string;
  email: string;
  firstname: string;
  lastname: string;
  middlesname?: string;
  phonenumber: string;
  job: string;

  companyId: string;

  status: ApproveStatusEnum;
  role: UserRoleEnum;
};
