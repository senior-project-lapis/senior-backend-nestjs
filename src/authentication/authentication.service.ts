import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcryptjs';
import { Request } from 'express';
import { UserInvalidCredentialException } from '../common/exception/user.invalid-credential.exception';
import { ConfigService } from '../config/config.service';
import { UserService } from '../user/user.service';
import { PublisherService } from '../publisher/publisher.service';
import { UserLoginInputDTO } from './dtos/user-login.input.dto';
import { UserSessionDTO } from './dtos/user-session.dto';
import { ApproveStatusEnum } from '../publisher/schema/enum/approve-status.enum';
import { UnapprovedPublisherException } from '../common/exception/unapproved-publisher.exception';
import { PublisherSessionDTO } from './dtos/publisher-session.dto';
import axios from 'axios';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly userService: UserService,
    private readonly publisherService: PublisherService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  private setToken(req: Request, token: string): void {
    req.res?.cookie('Authorization', `Bearer ${token}`, {
      httpOnly: true,
      domain: this.configService.get().main_hostname,
    });
  }

  private setFlarumToken(req: Request, token: string): void {
    req.res?.cookie('flarum_remember', token, {
      httpOnly: true,
      domain: this.configService.get().main_hostname,
    });
  }

  async publisherLogin(
    req: Request,
    args: UserLoginInputDTO,
  ): Promise<PublisherSessionDTO> {
    const user = await this.publisherService.getPublisherByEmail(args.email);
    const valid = await compare(args.password, user.password);
    const isApproved = user.status === ApproveStatusEnum.approved;

    if (!valid) throw new UserInvalidCredentialException();
    if (!isApproved) throw new UnapprovedPublisherException();

    const parsed = {
      ...user,
      password: undefined,
    };
    const token = await this.jwtService.signAsync(parsed);

    this.setToken(req, token);
    const session = req.session as Record<string, any>;
    session.user = parsed;

    return {
      ...user,
      _id: user._id,
    };
  }

  async userLogin(
    req: Request,
    args: UserLoginInputDTO,
  ): Promise<UserSessionDTO> {
    const user = await this.userService.getUserByEmail(args.email);
    const valid = await compare(args.password, user.password);
    if (!valid) throw new UserInvalidCredentialException();

    const parsed = {
      ...user,
      password: undefined,
    };
    const token = await this.jwtService.signAsync(parsed);

    this.setToken(req, token);

    const session = req.session as Record<string, any>;
    session.user = parsed;

    if (user.role === 'user') {
      // get flarum token
      const flarumData = this.configService.get().flarum;
      const queryUserBody = {
        identification: args.email,
        password: args.password,
        remember: 1,
      };
      const flarumHeaders = {
        headers: {
          Authorization:
            'Token ' + flarumData.apiKey + '; userId=' + flarumData.adminId,
        },
      };
      const flarumResponse = await axios.post(
        `${flarumData.url}/api/token`,
        queryUserBody,
        flarumHeaders,
      );
      if (!flarumResponse.data) {
        Logger.error('flarum fetch user error', 'flarum');
        throw new InternalServerErrorException('flarum user create error');
      }
      this.setFlarumToken(req, flarumResponse.data?.token);
    }

    return {
      ...user,
      _id: user._id,
    };
  }

  async refreshToken(req: Request): Promise<string> {
    const session = req.session as Record<string, any>;
    if (!session.user) throw new UserInvalidCredentialException();
    req.session.touch();
    const user = session.user;
    const token = await this.jwtService.signAsync(user);
    this.setToken(req, token);
    return 'refreshed';
  }
}
