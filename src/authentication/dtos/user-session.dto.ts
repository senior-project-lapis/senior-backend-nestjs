/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
import { UserSessionDto } from '../@types/user-session.type';
// enums
registerEnumType(UserRoleEnum, { name: 'UserRoleEnumSession' });

@ObjectType()
export class UserSessionDTO implements UserSessionDto {
  @Field(() => String)
  _id: string;

  @Field(() => String)
  email: string;

  @Field(() => String)
  firstname: string;

  @Field({ nullable: true })
  middlename?: string;

  @Field(() => String)
  lastname: string;

  @Field(() => String)
  dob: Date;

  @Field(() => String)
  phonenumber: string;

  @Field(() => String)
  address: string;

  // logic
  @Field(() => UserRoleEnum)
  role: UserRoleEnum;

  // @Field(() => String)
  // flarum_remember: string;
}
