/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { CompanySizeEnum } from '../../company/schema/enum/company-size.enum';
import { ApproveStatusEnum } from '../../publisher/schema/enum/approve-status.enum';
import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
import { PublisherSessionDto } from '../@types/publisher-session.type';
// enums
registerEnumType(UserRoleEnum, { name: 'UserRoleEnum' });
registerEnumType(CompanySizeEnum, { name: 'CompanySizeEnum' });
registerEnumType(ApproveStatusEnum, { name: 'ApproveStatusEnum' });

@ObjectType()
export class PublisherSessionDTO implements PublisherSessionDto {
  @Field(() => String)
  _id: string;

  @Field(() => String)
  email: string;

  @Field(() => String)
  firstname: string;

  @Field(() => String)
  middlename?: string;

  @Field(() => String)
  lastname: string;

  @Field(() => String)
  phonenumber: string;

  @Field(() => String)
  job: string;

  @Field(() => String)
  companyId: string;

  @Field(() => ApproveStatusEnum)
  status: ApproveStatusEnum;

  // logic
  @Field(() => UserRoleEnum)
  role: UserRoleEnum;
}
