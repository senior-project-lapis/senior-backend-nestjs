import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AppResolver } from './app.resolver';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigDatabaseService } from './config/config.database.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { UserModule } from './user/user.module';
import { PublisherModule } from './publisher/publisher.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { MongooseModule } from '@nestjs/mongoose';
import { JobModule } from './job/job.module';
import { CompanyModule } from './company/company.module';
import { FavoriteModule } from './favorite/favorite.module';
import { ReviewModule } from './review/review.module';
import { WatchingListModule } from './watching-list/watching-list.module';
import { EducationModule } from './education/education.module';
import { LocationModule } from './location/location.module';
import {
  ApolloFederationDriver,
  ApolloFederationDriverConfig,
} from '@nestjs/apollo';
import { DashboardModule } from './dashboard/dashboard.module';
@Module({
  imports: [
    ConfigModule,
    UserModule,
    PublisherModule,
    JobModule,
    CompanyModule,
    FavoriteModule,
    ReviewModule,
    WatchingListModule,
    EducationModule,
    LocationModule,
    DashboardModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useClass: ConfigDatabaseService,
    }),
    GraphQLModule.forRootAsync<ApolloFederationDriverConfig>({
      driver: ApolloFederationDriver,
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          path: '/graphql',
          cors: {
            origin: configService.get().origin,
            credentials: true,
          },
          installSubscriptionHandlers: false,
          introspection: true,
          autoSchemaFile: 'schema.gql',
          sortSchema: true,
          playground: {
            settings: {
              'request.credentials': 'include',
            },
          },
          context: ({ req, res, payload, connection }) => ({
            req,
            res,
            payload,
            connection,
          }),
        };
      },
    }),
    AuthenticationModule,
  ],
  controllers: [AppController],
  providers: [AppService, AppResolver],
})
export class AppModule {}
