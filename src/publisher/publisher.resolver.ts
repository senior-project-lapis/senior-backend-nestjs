import { UseFilters, UseGuards } from '@nestjs/common';
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Request } from 'express';
import { JwtAuthGuard } from '../authentication/guards/jwt-auth.guard';
import { PublisherStatus } from '../publisher/dtos/args/publisher-status.args.dto';
import { UserExceptionFilters } from '../common/filters/user-error.filter';
import { PublisherRegisterInputDto } from './dtos/inputs/publisher-register.input.dto';
import { PublisherUpdateInputDto } from './dtos/inputs/publisher-update.input.dto';
import {
  Publisher,
  PublisherDocumentWithCount,
  PublisherWithId,
} from './models/publisher.model';
import { PublisherService } from './publisher.service';

@Resolver(() => Publisher)
export class PublisherResolver {
  constructor(private readonly publisherService: PublisherService) {}

  // publisher
  @Query(() => PublisherWithId, { name: 'Publisher', nullable: true })
  async GetPublisher(@Args('id', { type: () => String }) id: string) {
    return this.publisherService.getById(id);
  }

  // publisher by session
  @UseGuards(JwtAuthGuard)
  @Query(() => PublisherWithId, { nullable: true })
  async GetPublisherBySession(@Context('req') req: Request) {
    const session = req.session as Record<string, any>;
    const id = session.user._id;
    return this.publisherService.getById(id);
  }

  @UseFilters(UserExceptionFilters)
  @Mutation(() => PublisherWithId)
  async CreatePublisher(
    @Args('PublisherRegisterInputDto')
    publisherRegisterInputDto: PublisherRegisterInputDto,
  ) {
    return this.publisherService.createPublisher(publisherRegisterInputDto);
  }

  @Mutation(() => PublisherWithId)
  async UpdatePublisher(
    @Args('PublisherUpdateInputDto')
    publisherUpdateInputDto: PublisherUpdateInputDto,
  ) {
    return this.publisherService.updatePublisher(publisherUpdateInputDto);
  }

  // publishers
  @Query(() => PublisherDocumentWithCount, { name: 'Publishers' })
  async GetPublishers(@Args() publisherStatus: PublisherStatus) {
    return this.publisherService.getPublishers(publisherStatus);
  }

  // @Mutation(() => Number)
  // async RemoveReview(@Args('id', { type: () => String }) id: string) {
  //   return this.publisherService.removePublisher(id);
  // }
}
