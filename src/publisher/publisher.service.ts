import {
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { genSalt, hash } from 'bcryptjs';
import { UserRoleEnum } from '../user/schema/enum/user-role.enum';
import {
  PublisherDocument,
  PublisherDocumentResponse,
} from './entity/publisher.entity';
import { LeanDocument, Model } from 'mongoose';

import { UserNotExistsException } from '../common/exception/user.not-exists.exception';
import { ConfigService } from '../config/config.service';
import { PublisherStatus } from './dtos/args/publisher-status.args.dto';
import { PublisherRegisterInputDto } from './dtos/inputs/publisher-register.input.dto';
import { PublisherUpdateInputDto } from './dtos/inputs/publisher-update.input.dto';
import {
  PublisherQueryReturns,
  PublishersQueryReturns,
} from './models/publisher-query-returns.model';
import { Publisher } from './models/publisher.model';
import { DuplcatedEmailException } from '../common/exception/duplicated-email.exception';
import { UserService } from '../user/user.service';
import { CompanyService } from '../company/company.service';
import { CreateCompanyInputDto } from '../company/dtos/inputs/create-company.input.dto';
import { ApproveStatusEnum } from './schema/enum/approve-status.enum';

@Injectable()
export class PublisherService {
  constructor(
    @InjectModel('publishers')
    private readonly publisherModel: Model<PublisherDocument>,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    @Inject(forwardRef(() => CompanyService))
    private readonly companyService: CompanyService,
    private readonly configService: ConfigService,
  ) {}

  async getPublishers(
    publisherStatus: PublisherStatus,
  ): Promise<PublisherDocumentResponse> {
    const { skip, limit, status, sortBy, sortOrder } = publisherStatus;
    const query = this.publisherModel.find();
    if (status) {
      query.where('status', status);
    }
    const response: PublisherDocumentResponse = {
      totalCount: await query.clone().count(),
      publisherResult: await query
        .select({ password: 0 })
        .sort([[sortBy, sortOrder]])
        .skip(skip)
        .limit(limit)
        .lean(),
    };
    return response;
  }

  async getPublishersCount(): Promise<number> {
    return this.publisherModel.countDocuments({ status: 'approved' });
  }

  async getWaitingApprovePublishersCount(): Promise<number> {
    return this.publisherModel.countDocuments({ status: 'pending' });
  }

  async getById(id: string): Promise<PublisherQueryReturns> {
    return this.publisherModel.findById(id).select({ password: 0 }).lean();
  }

  async checkExistPublisher(email: string): Promise<boolean> {
    let isExist: any = null;
    isExist = await this.publisherModel.exists({ email });
    return isExist;
  }

  async createPublisher(
    payload: PublisherRegisterInputDto,
  ): Promise<PublisherDocument> {
    const existUser = await this.userService.checkExistUser(payload.email);
    const existPublisher = await this.checkExistPublisher(payload.email);

    if (existUser || existPublisher)
      throw new DuplcatedEmailException('Found existing email');

    const { round } = this.configService.get().bcrypt;
    const salt = await genSalt(round);
    const publisher: Publisher = {
      status: ApproveStatusEnum.pending,
      ...payload,
      password: await hash(payload.password, salt),
      role: UserRoleEnum.publisher,

      createAt: new Date(),
      updateAt: new Date(),
    };

    let doc;
    try {
      doc = new this.publisherModel(publisher);
    } catch (err) {}

    const saved = await doc?.save();
    if (!saved) {
      Logger.error('publisher create error', 'MongoDB');
      throw new InternalServerErrorException('publisher create error');
    }
    return saved;
  }

  async updatePublisher(
    payload: PublisherUpdateInputDto,
  ): Promise<PublisherDocument> {
    const updatePayload = {
      ...payload,
      id: undefined,
      password: undefined,
      updateAt: new Date(),
    };
    const publisher = await this.publisherModel.findByIdAndUpdate(
      payload.id,
      updatePayload,
      {
        new: true,
      },
    );
    if (!publisher) throw new UserNotExistsException();
    return publisher;
  }

  async getPublisherByEmail(
    email: string,
  ): Promise<LeanDocument<PublisherDocument>> {
    const publisher = await this.publisherModel
      .findOne({ email: email })
      .lean();
    if (!publisher) throw new UserNotExistsException();
    return publisher;
  }
  async removePublisher(id: string): Promise<number> {
    const { deletedCount } = await this.publisherModel.deleteOne({ _id: id });
    return deletedCount;
  }
}
