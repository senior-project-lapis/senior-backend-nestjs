import { PublisherDocument } from '../entity/publisher.entity';

export type PublisherQueryReturns = Omit<PublisherDocument, 'password'> | null;
export type PublishersQueryReturns = Omit<PublisherDocument, 'password'>[] | [];
