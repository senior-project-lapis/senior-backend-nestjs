/* eslint-disable @typescript-eslint/no-unused-vars */
import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
import { ApproveStatusEnum } from '../schema/enum/approve-status.enum';
import { IPublisher } from '../interfaces/publisher';
import { CompanySizeEnum } from '../../company/schema/enum/company-size.enum';

// enums
registerEnumType(UserRoleEnum, { name: 'UserRoleEnum' });
registerEnumType(CompanySizeEnum, { name: 'CompanySizeEnum' });
registerEnumType(ApproveStatusEnum, { name: 'ApproveStatusEnum' });

@ObjectType()
export class Publisher implements IPublisher {
  @Field(() => String)
  email: string;

  @Field(() => String)
  password: string;

  @Field(() => String)
  firstname: string;

  @Field({ nullable: true })
  middlename?: string;

  @Field(() => String)
  lastname: string;

  @Field(() => String)
  phonenumber: string;

  @Field(() => String)
  job: string;

  @Field(() => String)
  companyId: string;

  @Field(() => ApproveStatusEnum)
  status: ApproveStatusEnum;

  // logic
  @Field(() => UserRoleEnum)
  role: UserRoleEnum;

  @Field(() => String)
  createAt: Date;

  @Field(() => String)
  updateAt: Date;
}

@ObjectType()
export class PublisherWithId extends Publisher {
  @Field(() => String)
  _id: string;
}

@ObjectType()
export class PublisherDocumentWithCount {
  @Field(() => [PublisherWithId])
  publisherResult: PublisherWithId[];

  @Field(() => Number)
  totalCount: number;
}
