import { Document, Schema } from 'mongoose';

import { PublisherSchema } from '../schema/publisher';
import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
import { ApproveStatusEnum } from '../schema/enum/approve-status.enum';
import { CompanySizeEnum } from '../../company/schema/enum/company-size.enum';

export const userRoleConstant: UserRoleEnum[] = [
  UserRoleEnum.admin,
  UserRoleEnum.user,
  UserRoleEnum.publisher,
];

export const approveStatusConstant: ApproveStatusEnum[] = [
  ApproveStatusEnum.approved,
  ApproveStatusEnum.pending,
  ApproveStatusEnum.rejected,
];

export const companySizeConstant: CompanySizeEnum[] = [
  CompanySizeEnum.large,
  CompanySizeEnum.medium,
  CompanySizeEnum.small,
];

const publisherSchema = new Schema<PublisherDocument>(
  {
    email: { type: String, unique: true },
    password: { type: String, required: true },
    firstname: { type: String, required: true },
    middlename: { type: String, required: false },
    lastname: { type: String, required: true },
    phonenumber: { type: String, required: true },
    job: { type: String, required: true },

    // company
    companyId: { type: String, required: true },

    status: { type: String, required: true, enum: approveStatusConstant },

    role: {
      type: String,
      require: true,
      enum: userRoleConstant,
    },

    createAt: { type: Date, default: Date.now, required: true },
    updateAt: { type: Date, default: Date.now, required: true },
  },
  { versionKey: false },
);

export type PublisherDocument = Document & PublisherSchema;

export type PublisherDocumentResponse = {
  publisherResult: PublisherDocument;
  totalCount: number;
};

export default publisherSchema;
