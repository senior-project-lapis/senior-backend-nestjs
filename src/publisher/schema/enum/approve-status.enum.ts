export enum ApproveStatusEnum {
  approved = 'approved',
  pending = 'pending',
  rejected = 'rejected',
}
