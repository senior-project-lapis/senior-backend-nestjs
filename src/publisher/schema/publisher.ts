import { CompanySizeEnum } from '../../company/schema/enum/company-size.enum';
import { UserRoleEnum } from '../../user/schema/enum/user-role.enum';
import { ApproveStatusEnum } from './enum/approve-status.enum';

export type PublisherSchema = {
  _id: string;

  email: string;
  password: string;
  firstname: string;
  middlename?: string;
  lastname: string;
  phonenumber: string;
  job: string;

  companyId: string;

  status: ApproveStatusEnum;

  role: UserRoleEnum;

  createAt: Date;
  updateAt: Date;
};
