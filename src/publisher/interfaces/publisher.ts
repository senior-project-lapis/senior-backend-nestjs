import { UserRoleEnum } from '../schema/enum/user-role.enum';
import { ApproveStatusEnum } from '../schema/enum/approve-status.enum';
import { CompanySizeEnum } from '../../company/schema/enum/company-size.enum';

export interface IPublisher {
  email: string;
  password: string;
  firstname: string;
  middlename?: string;
  lastname: string;
  phonenumber: string;
  job: string;

  companyId: string;
  status: ApproveStatusEnum;

  role: UserRoleEnum;

  createAt: Date;
  updateAt: Date;
}
