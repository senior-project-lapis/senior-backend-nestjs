/* eslint-disable @typescript-eslint/no-unused-vars */
import { ArgsType, Field, Int } from '@nestjs/graphql';
import { SkipLimitArgs } from '../../../common/dtos/args/skip-limit.args.dto';
import { SortOrderEnum } from '../../../common/enum/sort-order.enum';
import { ApproveStatusEnum } from '../../../publisher/schema/enum/approve-status.enum';

@ArgsType()
export class PublisherStatus extends SkipLimitArgs {
  @Field(() => String, { nullable: true })
  status: ApproveStatusEnum;

  @Field(() => String, { nullable: true })
  sortBy: string;

  @Field(() => String, { nullable: true })
  sortOrder: SortOrderEnum;
}
