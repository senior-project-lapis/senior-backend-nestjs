import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, MaxLength, MinLength, IsOptional } from 'class-validator';
import { CompanySizeEnum } from '../../../company/schema/enum/company-size.enum';
import { Publisher } from '../../models/publisher.model';
import { ApproveStatusEnum } from '../../schema/enum/approve-status.enum';

@InputType()
export class PublisherRegisterInputDto
  implements
    Pick<
      Publisher,
      | 'email'
      | 'password'
      | 'firstname'
      | 'middlename'
      | 'lastname'
      | 'phonenumber'
      | 'job'
      | 'companyId'
    >
{
  @Field()
  @IsEmail()
  email: string;

  @Field()
  @MinLength(6)
  @MaxLength(48)
  password: string;

  @Field()
  @MinLength(1)
  @MaxLength(48)
  firstname: string;

  @Field({ nullable: true })
  @MaxLength(48)
  @IsOptional()
  middlename: string;

  @Field()
  @MinLength(1)
  @MaxLength(48)
  lastname: string;

  @Field()
  @MinLength(10)
  phonenumber: string;

  @Field()
  @MinLength(1)
  @MaxLength(48)
  job: string;

  @Field({ nullable: true })
  companyId: string;
}
