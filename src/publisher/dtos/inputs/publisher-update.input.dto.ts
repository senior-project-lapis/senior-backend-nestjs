import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, MaxLength, MinLength, IsOptional } from 'class-validator';
import { PublisherRegisterInputDto } from './publisher-register.input.dto';
import { ApproveStatusEnum } from '../../schema/enum/approve-status.enum';

@InputType()
export class PublisherUpdateInputDto
  implements Omit<PublisherRegisterInputDto, 'password'>
{
  @Field({})
  id: string;

  @Field({ nullable: true })
  @IsEmail()
  @IsOptional()
  email: string;

  @Field({ nullable: true })
  @IsOptional()
  password: string;

  @Field({ nullable: true })
  @IsOptional()
  firstname: string;

  @Field({ nullable: true })
  @IsOptional()
  middlename: string;

  @Field({ nullable: true })
  @IsOptional()
  lastname: string;

  @Field({ nullable: true })
  @IsOptional()
  phonenumber: string;

  @Field({ nullable: true })
  @IsOptional()
  job: string;

  @Field({ nullable: true })
  @IsOptional()
  companyId: string;

  @Field({ nullable: true })
  @IsOptional()
  status: ApproveStatusEnum;
}
