import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import publisherSchema from './entity/publisher.entity';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ConfigModule } from '../config/config.module';
import { PublisherResolver } from './publisher.resolver';
import { PublisherService } from './publisher.service';
import { UserModule } from '../user/user.module';
import { CompanyModule } from '../company/company.module';

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature([
      {
        name: 'publishers',
        schema: publisherSchema,
      },
    ]),
    forwardRef(() => AuthenticationModule),
    forwardRef(() => UserModule),
    forwardRef(() => CompanyModule),
  ],
  providers: [PublisherService, PublisherResolver],
  exports: [PublisherService, ConfigModule],
})
export class PublisherModule {}
